#include "SpotLight.h"
#include "Constants.h"

using namespace DirectX::SimpleMath;

SpotLight::SpotLight() : 
	Entity(),
	m_ambientColor(0.1f, 0.1f, 0.1f, 1.0f),
	m_diffuseColor(1.0f, 1.0f, 1.0f, 1.0f),
	m_maxAngle(Constants::PI / 6)
{
	//Set default orientation to above origin pointing down
	Vector3 position(0.0f, 5.0f, 0.0f);

	//Use Axis angle constructor
	Vector3 axis(1.0f, 0.0f, 0.0f);
	float angle = Constants::PI / 2;
	Quaternion rotation = Quaternion::CreateFromAxisAngle(axis, angle);

	transform.SetPosition(position);
	transform.SetRotation(rotation);
}

SpotLight::SpotLight(
	const Transform& transform,
	const Color& ambientColor,
	const Color& diffuseColor,
	float maxAngle) : Entity(transform),
	m_ambientColor(ambientColor),
	m_diffuseColor(diffuseColor),
	m_maxAngle(maxAngle)
{
}

SpotLight::SpotLight(
	const Transform& transform) :
	Entity(transform),
	m_ambientColor(0.0f, 0.0f, 0.0f, 1.0f),
	m_diffuseColor(1.0f, 1.0f, 1.0f, 1.0f),
	m_maxAngle(1.0f)
{
}


SpotLight::~SpotLight()
{
}

const Color& SpotLight::getAmbientColor() const
{
	return m_ambientColor;
}
const Color& SpotLight::getDiffuseColor() const
{
	return m_diffuseColor;
}
float SpotLight::getMaxAngle() const
{
	return m_maxAngle;
}

void SpotLight::setAmbientColor(const Color& newAmbientColor)
{
	m_ambientColor = newAmbientColor;
}
void SpotLight::setDiffuseColor(const Color& newDiffuseColor)
{
	m_diffuseColor = newDiffuseColor;
}
void SpotLight::setMaxAngle(float newAngle)
{
	m_maxAngle = newAngle;
}

//Theta = thetaMax * sin(freq * time)
//dTheta/dt = thetaMax * freq * cos(freq * time)
//This sweeps the lights back and forth along the Z axis (rotates around X axis - therefore sweepX)
void SpotLight::sweepX(float deltaTime, float frequency, float maxDeflection)
{
	float angle = deltaTime * maxDeflection * frequency * cos(frequency * m_totalTime);
	DirectX::XMVECTOR rotationQuat = DirectX::XMQuaternionRotationAxis(DirectX::SimpleMath::Vector3::Right, angle);
	transform.Rotate(rotationQuat);
}

//This sweeps the lights back and forth along the X axis (rotates around Z axis - therefore sweepZ)
void SpotLight::sweepZ(float deltaTime, float frequency, float maxDeflection)
{
	float angle = deltaTime * maxDeflection * frequency * cos(frequency * m_totalTime);
	DirectX::XMVECTOR rotationQuat = DirectX::XMQuaternionRotationAxis(DirectX::SimpleMath::Vector3::Forward, angle);
	transform.Rotate(rotationQuat);
}

void SpotLight::passiveSpin(float deltaTime)
{
	m_totalTime += deltaTime;
	sweepX(deltaTime, 1.5f, 1.5f);
	sweepZ(deltaTime, 2.5f, 1.5f);
}

void SpotLight::passiveRecolor(float totalTime)
{
	Color randomizedColor = m_diffuseColor;
	//This computes a color for each light that's different than any other light
	//  It uses the light's unique combination of X and Z positions (0, 15 or -15)
	//  and the light's direction (which isn't unique) to give each light a slightly
	//  different color without having to use randomized values
	randomizedColor.x = abs(sin(totalTime + transform.GetRotation().x) / 2);
	randomizedColor.y = abs(cos(totalTime + getTransform().GetPosition().x) / 2);
	randomizedColor.z = abs(cos(totalTime + getTransform().GetPosition().z) / 2);
	setDiffuseColor(randomizedColor);
}

void SpotLight::loadData(PixelShaderCBuffer::SpotLight& dataContainer) const
{
	dataContainer.ambientColor = m_ambientColor;
	dataContainer.diffuseColor = m_diffuseColor;
	DirectX::XMFLOAT3 pos = transform.GetPosition();
	dataContainer.position = Vector3(pos.x, pos.y, pos.z);
	DirectX::XMStoreFloat3(&dataContainer.direction, transform.GetForwardVector());
	dataContainer.maxAngle = m_maxAngle;
}
