#pragma once

#include <DirectXMath.h>
#include <SimpleMath.h>


struct ParticleVertex 
{
	// Data all base particles will have 
	int Type;
	float Age;
	DirectX::XMFLOAT3 StartPosition;
	DirectX::XMFLOAT3 StartVelocity;
	DirectX::XMFLOAT4 StartColor;
	DirectX::XMFLOAT4 MidColor;
	DirectX::XMFLOAT4 EndColor;
	DirectX::XMFLOAT3 StartMidEndSize;
};