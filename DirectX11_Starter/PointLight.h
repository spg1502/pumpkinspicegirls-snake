#pragma once
#include "Entity.h"
#include "ShaderConstants.h"

class PointLight :
	public Entity
{
public:
	PointLight();
	PointLight(
		const Transform& transform,
		const DirectX::SimpleMath::Color& ambientColor,
		const DirectX::SimpleMath::Color& diffuseColor);
	PointLight(const Transform& transform);
	virtual ~PointLight();

	const DirectX::SimpleMath::Color& getAmbientColor() const;
	const DirectX::SimpleMath::Color& getDiffuseColor() const;

	void setAmbientColor(const DirectX::SimpleMath::Color& newAmbientColor);
	void setDiffuseColor(const DirectX::SimpleMath::Color& newDiffuseColor);

	void loadData(PixelShaderCBuffer::PointLight& dataContainer) const;

private:
	DirectX::SimpleMath::Color m_ambientColor;
	DirectX::SimpleMath::Color m_diffuseColor;
};