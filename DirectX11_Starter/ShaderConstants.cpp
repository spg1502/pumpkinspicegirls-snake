#include "ShaderConstants.h"

#include "SimpleShader.h"

ShaderConstants::ShaderConstants()
{
	ZeroMemory(this, sizeof(ShaderConstants));
}

bool ShaderConstants::LoadIntoShaders(SimpleVertexShader& vertexShader, SimplePixelShader& pixelShader)
{
	bool result = true;
	result &= vShaderCBuffer.LoadIntoShader(vertexShader);
	result &= pShaderCBuffer.LoadIntoShader(pixelShader);
	return result;
}

bool VertexShaderCBuffer::LoadIntoShader(SimpleVertexShader& vertexShader)
{
	bool result = true;
	result &= vertexShader.SetMatrix4x4("world", world);
	result &= vertexShader.SetMatrix4x4("view", view);
	result &= vertexShader.SetMatrix4x4("projection", projection);
	return result;
}

bool PixelShaderCBuffer::LoadIntoShader(SimplePixelShader& pixelShader)
{
	bool result;
	result &= pixelShader.SetData("directionalLights", &directionalLights, sizeof(DirectionalLight) * 2);
	result &= pixelShader.SetData("pointLights", &pointLights, sizeof(pointLights) * 1);
	result &= pixelShader.SetData("spotLights", &spotLights, sizeof(spotLights) * 1);

	result &= pixelShader.SetShaderResourceView("diffuseTexture", diffuseTexture);
	result &= pixelShader.SetSamplerState("basicSampler", basicSampler);
	return result;
}
