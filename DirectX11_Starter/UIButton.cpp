#include "UIButton.h"

UIButton::UIButton(std::shared_ptr<Texture> tex, std::shared_ptr<DirectX::SpriteFont> sF, wchar_t* te, 
	Vector2 pos, float rot, Vector2 sca) :
	UIElement(tex, pos, rot, sca), spriteFont(sF), text(te)
{
	//divides the string's width and height by 2.0f to get the origin in the middle
	textOrigin = DirectX::XMVectorDivide(spriteFont->MeasureString(text), DirectX::XMVectorSet(2.0f, 2.0f, 0.0f, 0.0f));
}

UIButton::~UIButton()
{

}

void UIButton::Draw(std::unique_ptr<DirectX::SpriteBatch> const &sb)
{
	UIElement::Draw(sb);
	spriteFont->DrawString(sb.get(), text, position, DirectX::Colors::Black, 0.0f, textOrigin, 1.0f);
}

void UIButton::SetText(wchar_t* t)
{
	text = t;
}
