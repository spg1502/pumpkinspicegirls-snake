#include "PointLight.h"

PointLight::PointLight() : Entity()
{
}

PointLight::PointLight(const Transform & transform, const DirectX::SimpleMath::Color & ambientColor, const DirectX::SimpleMath::Color & diffuseColor) :
	Entity(transform),
	m_ambientColor(ambientColor),
	m_diffuseColor(diffuseColor)
{
}

PointLight::PointLight(const Transform & transform) :
	Entity(transform),
	m_ambientColor(0.1f, 0.1f, 0.1f, 0.1f),
	m_diffuseColor(1.0f, 1.0f, 1.0f, 1.0f)
{
}

PointLight::~PointLight()
{
}

const DirectX::SimpleMath::Color & PointLight::getAmbientColor() const
{
	return m_ambientColor;
}

const DirectX::SimpleMath::Color & PointLight::getDiffuseColor() const
{
	return m_diffuseColor;
}

void PointLight::setAmbientColor(const DirectX::SimpleMath::Color & newAmbientColor)
{
	m_ambientColor = newAmbientColor;
}

void PointLight::setDiffuseColor(const DirectX::SimpleMath::Color & newDiffuseColor)
{
	m_diffuseColor = newDiffuseColor;
}

void PointLight::loadData(PixelShaderCBuffer::PointLight & dataContainer) const
{
	dataContainer.ambientColor = m_ambientColor;
	dataContainer.diffuseColor = m_diffuseColor;
	dataContainer.position = transform.GetPosition();
}
