//Order of matrix multiplication is whatever makes sense, if you think it would make more sense in another order, don't hesitate to bring it up
//CopyAllBufferData brings in the data you put into the vertexShader onto the GPU for drawing
//Transposing matricies is the last step for all world matricies, after doing all the calculations, THEN transpose them
// If you ever use non-uniform scales the light normals for your object will be screwed up
//		When you do a non-uniform scale, calculate the inverse transpose of the world matrix of that object and then pass it into the vertex shader as an extra veriable in the constant buffer

//Limit camera's rotation around X axis, don't let the user flip the camera forwards/backwards
//MoveRelative(float x, float y, float z); function for the camera, move relative to the camera's orientation
//MoveAbsolute(float x, float y, float z); function for the camera, move to an absolute position in space
//Point Lights
//Lights with variable amounts of specularity - Be sure to pass camera position into the pixel shader for the reflective angle
//Moving lights
//	--BE CONSCIOUS OF DATA PACKING--
#pragma once

#include <d3d11.h>
#include <SimpleMath.h>
#include "Mesh.h"
#include "Material.h"
#include "DirectionalLightStruct.h"
#include "WICTextureLoader.h"
#include "Transform.h"

struct ShaderConstants;

class Entity
{
public:
	Entity();
	Entity(
		Mesh* pModel,
		Material* pMaterial,
		const DirectX::SimpleMath::Vector3& pPosition,
		const DirectX::SimpleMath::Quaternion& pRotation,
		const DirectX::SimpleMath::Vector3& pScale);
	Entity(Transform t);

	~Entity();

	void Draw(ShaderConstants& shaderConstData);

	Material* GetMaterial() const;
	void SetMaterial(Material* newMaterial);
	Mesh* GetMesh() const;
	void SetMesh(Mesh* newMesh);
	
	const Transform& getTransform() const;
	Transform& getTransform();

protected:
	Transform transform;				//This is the only class member that will be staying

private:

	Mesh* m_model;

	Material* m_material;
};

