#include "FoodPellet.h"

Material* FoodPellet::RedSoloCupTexture;
Material* FoodPellet::GreenSoloCupTexture;

namespace 
{
	float randInRange(float min, float max)
	{
		return rand() / (RAND_MAX / (max - min)) + min;
	}
	Vector3 randInArea(float w, float h, float yDisPlace)
	{
		float x = -(w * .5) + rand() / (RAND_MAX / w);
		float z = -(h * .5) + rand() / (RAND_MAX / h);
		return Vector3(x, yDisPlace, z);
	}
}


void FoodPellet::InitMaterials(Material * redMaterial, Material * greenMaterial)
{
	RedSoloCupTexture = redMaterial;
	GreenSoloCupTexture = greenMaterial;
}

FoodPellet::FoodPellet()
{
}


FoodPellet::FoodPellet(
	Mesh* mesh, 
	Material* material,
	float width,
	float height) :
	width(width),
	height(height),
	type(PelletType::Standard)
{
	// Spawing entity in the initalizer list causes issues with constant variables, so create entity here instead 
	entity = Entity(mesh, material, randInArea(width, height, Y_DISPLACEMENT), DirectX::SimpleMath::Quaternion::Identity, DirectX::SimpleMath::Vector3(1.0f));
}

FoodPellet::~FoodPellet()
{

}


bool FoodPellet::DidCollide(float r, const Vector3& p) const
{
	// Move to new location when you collide 
	DirectX::XMFLOAT3 ep = entity.getTransform().GetPosition(); 
	return CollisionDetection::CircleCollision(r, p, RADIUS, Vector3(ep.x, ep.y, ep.z));
}



void FoodPellet::Reset()
{
	Vector3 rp = randInArea(width, height, Y_DISPLACEMENT);
	entity.getTransform().SetPosition(rp);


	int pelletType = rand() % 10;
	switch (pelletType)
	{
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
		type = PelletType::Speed;
		entity.SetMaterial(GreenSoloCupTexture);
		break;
	default:
		type = PelletType::Standard;
		entity.SetMaterial(RedSoloCupTexture);
		break;
	}
}

void FoodPellet::Draw(ShaderConstants& shaderConstData)
{
	entity.Draw(shaderConstData);
}

PelletType FoodPellet::getType() const
{
	return type;
}

void FoodPellet::SetType(PelletType newType)
{
	type = newType;
}

DirectX::SimpleMath::Vector3 FoodPellet::GetPos() const
{
	return entity.getTransform().GetPosition(); 
}