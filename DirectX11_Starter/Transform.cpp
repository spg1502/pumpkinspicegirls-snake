#include "Transform.h"

using namespace DirectX;

Transform::Transform()
	:position{ 0, 0, 0 }, scale{ 1.0f, 1.0f, 1.0f }
{
	XMStoreFloat4(&rotation, XMQuaternionIdentity());
}
Transform::Transform(const XMFLOAT3& position)
	: position{ position }, scale{ XMFLOAT3{ 1.0f, 1.0f, 1.0f } }
{
	XMStoreFloat4(&rotation, XMQuaternionIdentity());
}
Transform::Transform(const XMFLOAT3& position, const XMFLOAT4& rotation, const XMFLOAT3& scale)
	: position{ position }, rotation{ rotation }, scale{ scale }
{

}

Transform::~Transform()
{
}

void Transform::SetPosition(const XMFLOAT3& p)
{
	position = p;
}
void Transform::SetScale(const XMFLOAT3& s)
{
	scale = s;
}
void Transform::SetRotation(const XMFLOAT4& r)
{
	rotation = r;
}
void Transform::SetRotation(const XMVECTOR& r)
{
	XMStoreFloat4(&rotation, r);
}

void Transform::Translate(const XMFLOAT3& offset)
{
	position.x += offset.x;
	position.y += offset.y;
	position.z += offset.z;
}
void Transform::Rotate(const XMFLOAT4& offset)
{
	XMVECTOR tempOffset = XMLoadFloat4(&offset);
	XMVECTOR originalOrientation = XMLoadFloat4(&rotation);
	XMStoreFloat4(&rotation, XMQuaternionMultiply(originalOrientation, tempOffset));
}
void Transform::Translate(const XMVECTOR& offset)
{
	auto tempPosition = XMLoadFloat3(&position);
	tempPosition += offset;
	XMStoreFloat3(&position, tempPosition);
}
void Transform::Rotate(const XMVECTOR& offset)
{
	XMVECTOR originalOrientation = XMLoadFloat4(&rotation);
	XMStoreFloat4(&rotation, XMQuaternionMultiply(originalOrientation, offset));
}

const XMFLOAT3& Transform::GetPosition() const
{
	return position;
}
const XMFLOAT3& Transform::GetScale() const
{
	return scale;
}
const XMFLOAT4& Transform::GetRotation() const
{
	return rotation;
}
XMFLOAT4X4 Transform::GetMatrix() const
{
	XMVECTOR rotationQuat = XMLoadFloat4(&rotation);
	XMMATRIX translation = XMMatrixTranslation(position.x, position.y, position.z);
	XMMATRIX scaleMat = XMMatrixScaling(scale.x, scale.y, scale.z);
	XMMATRIX rotationMatrix = XMMatrixRotationQuaternion(rotationQuat);
	XMFLOAT4X4 tempMatrix;
	XMStoreFloat4x4(&tempMatrix, scaleMat * rotationMatrix * translation);
	return tempMatrix;
}
XMVECTOR Transform::GetForwardVector() const
{
	XMVECTOR orientation = XMLoadFloat4(&rotation);
	XMVECTOR forward = XMLoadFloat3(&worldForward);
	return XMVector3Rotate(forward, orientation);
}
XMVECTOR Transform::GetRightVector() const
{
	XMVECTOR orientation = XMLoadFloat4(&rotation);
	XMVECTOR right = XMLoadFloat3(&worldRight);
	return XMVector3Rotate(right, orientation);
}
XMVECTOR Transform::GetUpVector() const
{
	XMVECTOR orientation = XMLoadFloat4(&rotation);
	XMVECTOR up = XMLoadFloat3(&worldUp);
	return XMVector3Rotate(up, orientation);
}