// ----------------------------------------------------------------------------
//  A few notes on project settings
//
//  - The project is set to use the UNICODE character set
//    - This was changed in Project Properties > Config Properties > General > Character Set
//    - This basically adds a "#define UNICODE" to the project
//
//  - The include directories were automagically correct, since the DirectX 
//    headers and libs are part of the windows SDK
//    - For instance, $(WindowsSDK_IncludePath) is set as a project include 
//      path by default.  That's where the DirectX headers are located.
//
//  - Two libraries had to be manually added to the Linker Input Dependencies
//    - d3d11.lib
//    - d3dcompiler.lib
//    - This was changed in Project Properties > Config Properties > Linker > Input > Additional Dependencies
//
//  - The Working Directory was changed to match the actual .exe's 
//    output directory, since we need to load the compiled shader files at run time
//    - This was changed in Project Properties > Config Properties > Debugging > Working Directory
//
//
//
//	- EXAMPLE CODE CHANGE Number 2
// ----------------------------------------------------------------------------

#include "MyDemoGame.h"
#include "Vertex.h"
#include "SimpleShader.h"
#include "WICTextureLoader.h"

// For the DirectX Math library
using namespace DirectX;


#pragma region Win32 Entry Point (WinMain)
// --------------------------------------------------------
// Win32 Entry Point - Where this program starts
// --------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	// Create the game object.
	MyDemoGame game(hInstance);
	
	// This is where we'll create the window, initialize DirectX, 
	// set up geometry and shaders, etc.
	if( !game.Init() )
		return 0;
	
	// All set to run the game loop
	return game.Run();
}

#pragma endregion

#pragma region Constructor / Destructor
// --------------------------------------------------------
// Base class constructor will set up all of the underlying
// fields, and then we can overwrite any that we'd like
// --------------------------------------------------------
MyDemoGame::MyDemoGame(HINSTANCE hInstance) 
	: DirectXGameCore(hInstance)
{
	// Set up a custom caption for the game window.
	// - The "L" before the string signifies a "wide character" string
	// - "Wide" characters take up more space in memory (hence the name)
	// - This allows for an extended character set (more fancy letters/symbols)
	// - Lots of Windows functions want "wide characters", so we use the "L"
	windowCaption = L"Snake - Pumpkin Spice Girls";

	// Custom window size - will be created by Init() later
	windowWidth = 800;
	windowHeight = 600;

	mouseDown = false;
}

// --------------------------------------------------------
// Cleans up our DirectX stuff and any objects we need to delete
// - When you make new DX resources, you need to release them here
// - If you don't, you get a lot of scary looking messages in Visual Studio
// --------------------------------------------------------
MyDemoGame::~MyDemoGame()
{
	// Delete our simple shaders
	delete vertexShader;
	delete pixelShader;
	delete floorShader;

	// Delete our camera
	delete camera;

	// Delete any materials created
	delete material1;
	delete floorBgMat;

	// Delete snake 
	delete snake; 

	// Clean up solo cup data 
	delete redSoloCupMat;
	delete greenSoloCupMat;
	delete soloCupHeadMat;
	delete soloCupMesh; 
	delete soloCupGroupMesh; 

	delete floorMat;
	delete floorMesh; 

	delete particleManager; 
}

#pragma endregion

#pragma region Initialization

// --------------------------------------------------------
// Initializes the base class (including the window and D3D),
// sets up our geometry and loads the shaders (among other things)
// --------------------------------------------------------
bool MyDemoGame::Init()
{
	currentGameState = mainMenu;
	// Call the base class's Init() method to create the window,
	// initialize DirectX, etc.
	if( !DirectXGameCore::Init() )
		return false;

	spriteBatch.reset(new SpriteBatch(deviceContext));

	// Set up all the data for each of our directional lights
	// Be sure to set the W value of the direction to 0
	DirectionalLight dLightUno = {
		Color(0.1f,  0.1f,  0.1f, 1.0f), //Ambient color
		Color(1.0f,  1.0f, 0.0f, 1.0f), //Diffuse color
		Vector3(-1.0f, 0.0f,  0.0f)	//Direction
	};

	DirectionalLight dLightDos = {
		Color(0.1f,	0.1f,	0.1f,	1.0f),
		Color(0.0f,	1.0f,	1.0f,	1.0f),
		Vector3(1.0f, 0.0f,	0.0f)
	};

	// Add our directional lights to our array.
	// IMPORTANT: The size of the directional light array in PixelShader.hlsl MUST BE THE SAME SIZE AS THIS ARRAY
	dLights.push_back(dLightUno);
	dLights.push_back(dLightDos);

	DirectX::XMFLOAT4 downRotation(cos(XM_PIDIV4), 0, 0, sin(XM_PIDIV4));
	SpotLight sLight01 = {Transform(DirectX::XMFLOAT3(15.0f, 2.0f, -15.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight02 = {Transform(DirectX::XMFLOAT3(15.0f, 2.0f, 0.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight03 = {Transform(DirectX::XMFLOAT3(15.0f, 2.0f, 15.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight04 = {Transform(DirectX::XMFLOAT3(0.0f, 2.0f, -15.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight05 = {Transform(DirectX::XMFLOAT3(0.0f, 2.0f, 0.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight06 = {Transform(DirectX::XMFLOAT3(0.0f, 2.0f, 15.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight07 = {Transform(DirectX::XMFLOAT3(-15.0f, 2.0f, -15.0f),downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight08 = {Transform(DirectX::XMFLOAT3(-15.0f, 2.0f, 0.0f),	downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	SpotLight sLight09 = {Transform(DirectX::XMFLOAT3(-15.0f, 2.0f, 15.0f), downRotation, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))};
	
	// Add our spot lights into our array we loop through when loading light data into the pixel/vertex shaders
	sLights.push_back(sLight01);
	sLights.push_back(sLight02);
	sLights.push_back(sLight03);
	sLights.push_back(sLight04);
	sLights.push_back(sLight05);
	sLights.push_back(sLight06);
	sLights.push_back(sLight07);
	sLights.push_back(sLight08);
	sLights.push_back(sLight09);

	//Section Point Light Initialization

	//End Section Point Light Initialization

	// Helper methods to create something to draw, load shaders to draw it 
	// with and set up matrices so we can see how to pass data to the GPU.
	LoadShaders();
	CreateGeometry();
	CreateMatrices();

	// Tell the input assembler stage of the pipeline what kind of
	// geometric primitives we'll be using and how to interpret them
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Set up the snake game
	snake = new Snake(soloCupMesh, soloCupMesh, soloCupGroupMesh, soloCupHeadMat, redSoloCupMat, redSoloCupMat);
	camera->SetEntityToFollow(snake->getHead());

	// Set up the particle manager 
	particleManager = new ParticleManager(device, deviceContext, L"../Assets/confetti.png");

	floor = Entity(floorMesh, floorMat, DirectX::SimpleMath::Vector3(0, -.5, 0), DirectX::SimpleMath::Quaternion::Identity, DirectX::SimpleMath::Vector3(30));
	floorBg.reset(new Entity(floorMesh, floorBgMat, DirectX::SimpleMath::Vector3(0, -1.6f, 0), DirectX::SimpleMath::Quaternion::Identity, DirectX::SimpleMath::Vector3(60.0f)));

	m_states.reset(new CommonStates(device));

	//setup game UI elements
	SetupUI();

	// Successfully initialized
	return true;
}

// --------------------------------------------------------
// Loads shaders from compiled shader object (.cso) files
// - These simple shaders provide helpful methods for sending
//   data to individual variables on the GPU
// --------------------------------------------------------
void MyDemoGame::LoadShaders()
{
	vertexShader = new SimpleVertexShader(device, deviceContext);
	vertexShader->LoadShaderFile(L"../Assets/VertexShader.cso");

	pixelShader = new SimplePixelShader(device, deviceContext);
	pixelShader->LoadShaderFile(L"../Assets/PixelShader.cso");

	//Initialize any default values for the floor shader here
	floorShader = new SimplePixelShader(device, deviceContext);
	floorShader->LoadShaderFile(L"../Assets/FloorShader.cso");
	floorShader->SetFloat("timePeriod", 5.0f);
	floorShader->SetFloat("floorScaleAmount", 0.2f);
	floorShader->SetFloat("rotationScalar", 0);
	floorShader->SetFloat("tileWidth", 4.0f);
}

// --------------------------------------------------------
// Creates the geometry we're going to draw - a single triangle for now
// --------------------------------------------------------
void MyDemoGame::CreateGeometry()
{
	// Create a material
	material1 = new Material(vertexShader, pixelShader, device);
	material1->LoadDiffuseMap(device, deviceContext, L"../Assets/carpetTexture.png");

	// Create mesh/material for a solo cup
	soloCupMesh = new Mesh("../Assets/SoloCup.obj", device, deviceContext); 
	redSoloCupMat = new Material(vertexShader, pixelShader, device);
	greenSoloCupMat = new Material(vertexShader, pixelShader, device);
	redSoloCupMat->LoadDiffuseMap(device, deviceContext, L"../Assets/SoloCup_Diffuse.png");
	greenSoloCupMat->LoadDiffuseMap(device, deviceContext, L"../Assets/GreenSoloCup_Diffuse.png");

	//Stuff I'm not proud of
	FoodPellet::InitMaterials(redSoloCupMat, greenSoloCupMat);

	soloCupHeadMat = new Material(vertexShader, pixelShader, device);
	soloCupHeadMat->LoadDiffuseMap(device, deviceContext, L"../Assets/SnakeHead_Diffuse.png");
	soloCupGroupMesh = new Mesh("../Assets/SoloCupGroup.obj", device, deviceContext);

	//Create floor object data
	floorMesh = new Mesh("../Assets/Plain_Horizontal.obj", device, deviceContext); 
	floorMat = new Material(vertexShader, floorShader, device);

	floorBgMat = new Material(vertexShader, pixelShader, device);
	//floorBgMat->LoadDiffuseMap(device, deviceContext, L"../Assets/dance_floor.png");
	floorBgMat->LoadDiffuseMap(device, deviceContext, L"../Assets/floor_bg.png");

	XMFLOAT3 forwardVector = { 0, 0, -1 };	//Default forward vector is pointing at the default camera position
											//		this defines the vector the object initially moves along for
											//		the MoveForward method, any entity translations are applied
	XMFLOAT3 upVector = { 0, 1, 0 };		//Default up vector is pointing up
											//		this is used to move entities relatively up, and to calculate
											//		the right vector for the MoveRight method

	//Set up a new camera object
	camera = new Camera(aspectRatio);
}

// --------------------------------------------------------
// Initializes the matrices necessary to represent our geometry's 
// transformations and our 3D camera
// --------------------------------------------------------
void MyDemoGame::CreateMatrices()
{
	// Set up world matrix
	// - In an actual game, each object will need one of these and they should
	//   update when/if the object moves (every frame)
	XMMATRIX W = XMMatrixIdentity();
	XMStoreFloat4x4(&worldMatrix, XMMatrixTranspose(W)); // Transpose for HLSL!

	// Create the View matrix
	// - In an actual game, recreate this matrix when the camera 
	//    moves (potentially every frame)
	// - We're using the LOOK TO function, which takes the position of the
	//    camera and the direction you want it to look (as well as "up")
	// - Another option is the LOOK AT function, to look towards a specific
	//    point in 3D space
	XMVECTOR pos = XMVectorSet(0, 0, -5, 0);
	XMVECTOR dir = XMVectorSet(0, 0, 1, 0);
	XMVECTOR up  = XMVectorSet(0, 1, 0, 0);
	XMMATRIX V   = XMMatrixLookToLH(
		pos,     // The position of the "camera"
		dir,     // Direction the camera is looking
		up);     // "Up" direction in 3D space (prevents roll)
	XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(V)); // Transpose for HLSL!

	// Create the Projection matrix
	// - This should match the window's aspect ratio, and also update anytime
	//   the window resizes (which is already happening in OnResize() below)
	XMMATRIX P = XMMatrixPerspectiveFovLH(
		0.25f * 3.1415926535f,		// Field of View Angle
		aspectRatio,				// Aspect ratio
		0.1f,						// Near clip plane distance
		100.0f);					// Far clip plane distance
	XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P)); // Transpose for HLSL!
}

void MyDemoGame::LoadTexture(const wchar_t* file, std::shared_ptr<Texture> tex)
{
	Microsoft::WRL::ComPtr<ID3D11Resource> res;
	CreateWICTextureFromFile(device, deviceContext, file, res.GetAddressOf(), tex->svr.ReleaseAndGetAddressOf());
	res.As(&tex->texture);
}

void MyDemoGame::SetupUI()
{
	start_tex = std::make_shared<Texture>();
	LoadTexture(L"../Assets/start_btn.png", start_tex);
	exit_tex = std::make_shared<Texture>();
	LoadTexture(L"../Assets/exit_btn.png", exit_tex);
	backgroundTex = std::make_shared<Texture>();
	LoadTexture(L"../Assets/snake_Bg.jpg", backgroundTex);

	spriteFont.reset(new DirectX::SpriteFont(device, L"../Assets/font.spritefont"));
	Vector2 sca = Vector2(0.25f, 0.25f);

	//load buttons
	startBtn.reset(new UIButton(start_tex, spriteFont, L"Start", Vector2(windowWidth * 0.5f, windowHeight * 0.5f), 0.0f, sca));
	exitBtn.reset(new UIButton(exit_tex, spriteFont, L"Exit", Vector2(windowWidth * 0.5f, windowHeight * 0.8f), 0.0f, sca));

	//load background
	backGround.reset(new Animation(Vector2(0.0f, 0.0f), 0.0f, 1.3f, 0.0f));
	backGround->Load(backgroundTex, 53, 21, 660, 495);
}

#pragma endregion

#pragma region Window Resizing

// --------------------------------------------------------
// Handles resizing DirectX "stuff" to match the (usually) new
// window size and updating our projection matrix to match
// --------------------------------------------------------
void MyDemoGame::OnResize()
{
	// Handle base-level DX resize stuff
	DirectXGameCore::OnResize();

	// Update our projection matrix since the window size changed
	if (camera != NULL)
	{
		camera->SetAspectRatio(aspectRatio);
		XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(XMLoadFloat4x4(&camera->GetProjectionMatrix()))); // Transpose for HLSL!
	};

	if (startBtn != nullptr && exitBtn != nullptr)
	{
		startBtn->SetPosition(Vector2(windowWidth * 0.5f, windowHeight * 0.5f));
		exitBtn->SetPosition(Vector2(windowWidth * 0.5f, windowHeight * 0.8f));
	}


}
#pragma endregion

#pragma region Game Loop

// --------------------------------------------------------
// Update your game here - take input, move objects, etc.
// --------------------------------------------------------
void MyDemoGame::UpdateScene(float deltaTime, float totalTime)
{
	// Quit if the escape key is pressed
	if (keyStateTracker->IsKeyPressed(keyboard->Escape))
		Quit();

	backGround->Update(deltaTime);

	//deal with gameplay and pause in UpdateGame
	if (currentGameState == gamePlay)
	{
		UpdateGame(deltaTime, totalTime);
	}
	else
	{
		//deal with main menu and end state in UpdateMenu
		UpdateMenu(deltaTime, totalTime);
	}

}

void MyDemoGame::UpdateMenu(float deltaTime, float totalTime)
{
	//Deal with main menu here
	if (currentGameState == mainMenu)
	{
		CheckButtons();
	}
	else
	{
		//do end stuff here
	}

}

void MyDemoGame::UpdateGame(float deltaTime, float totalTime)
{
	if (keyStateTracker->pressed.P)
	{
		paused = !paused;
	}

	if (paused) return;

	//Here is where we can modify entity positions, camera positions and light variables

	//Recoloring lights:
	for (DirectionalLight& light : dLights)
	{
		light.Update(totalTime);
	}
	for (unsigned int i = 0; i < sLights.size(); ++i)
	{
		sLights[i].passiveSpin(deltaTime);
		sLights[i].passiveRecolor(totalTime);
	}

	// Snake game logic - perform and save returned int
	// Snake.Update returns an int to share nessary information 
	// 0 - Alive (nothing special)
	// 1 - Dead 
	// 2 - Picked up food this frame 
	int result = snake->Update(deltaTime);

	// Snake dies
	if (result == 1 || IsOutOfBounds())
	{
		currentGameState = mainMenu;
		snake->Reset();

		// clean up left over particles 
		particleManager->CleanUp();
	}
	// Snake eats food
	if (result == 2)
	{
		particleManager->SpawnConfettiEffect(snake->GetHeadPos(), snake->GetFoodPos(), device, deviceContext);
	}

	//Give the camera a chance to respond to keyboard input
	camera->Update(deltaTime);
}

bool MyDemoGame::IsOutOfBounds()
{
	Vector3 headPos = snake->GetHeadPos();

	Vector3 maxPoint = floor.GetMesh()->MaxPoint();
	XMFLOAT4X4 floorTransform = floor.getTransform().GetMatrix();
	Matrix floorTransformMat(reinterpret_cast<float*>(&floorTransform));
	maxPoint = Vector3::Transform(maxPoint, floorTransformMat);
	Vector3 minPoint = floor.GetMesh()->MinPoint();
	minPoint = Vector3::Transform(minPoint, floorTransformMat);

	return (headPos.x > maxPoint.x || headPos.x < minPoint.x ||
		headPos.z > maxPoint.z || headPos.z < minPoint.z);
}

void MyDemoGame::CheckButtons()
{
	//when the left mouse button is clicked, check buttons
	if (mouseStateTracker->leftButton == mouseStateTracker->RELEASED)
	{
		if (startBtn->Clicked(mouseState->x, mouseState->y))
		{
			currentGameState = gamePlay;
			//reset the depth stencil state after using spritebatch
			deviceContext->OMSetDepthStencilState(NULL, 0);
		}
		
		else if (exitBtn->Clicked(mouseState->x, mouseState->y))
		{
			Quit();
		}
		
	}
}

// --------------------------------------------------------
// Clear the screen, redraw everything, present to the user
// --------------------------------------------------------
void MyDemoGame::DrawScene(float deltaTime, float totalTime)
{
	//Rotate the floor around the snake when it is turning. Uses lerp to get a smooth transition
	floorRotation += (snake->GetAngularVelocity() * 2.0f - floorRotation) * deltaTime * 1.5f;
	floorShader->SetFloat("rotationScalar", floorRotation);
	floorShader->SetFloat("totalTime", totalTime);
	floorShader->SetFloat2("snakePosition", DirectX::XMFLOAT2(snake->GetHeadPos().x, snake->GetHeadPos().z));

	// Background color (Cornflower Blue in this case) for clearing
	const float color[4] = {0.4f, 0.6f, 0.75f, 0.0f};

	// Clear the render target and depth buffer (erases what's on the screen) once per frame before any draw commands
	deviceContext->ClearRenderTargetView(renderTargetView, color);
	deviceContext->ClearDepthStencilView(
		depthStencilView, 
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);

	//Set the constants that the shaders will all use in a passable struct.
	ShaderConstants constData;
	XMStoreFloat4x4(&constData.vShaderCBuffer.view, XMMatrixTranspose(XMLoadFloat4x4(&camera->GetViewMatrix())));
	XMStoreFloat4x4(&constData.vShaderCBuffer.projection, XMMatrixTranspose(XMLoadFloat4x4(&camera->GetProjectionMatrix())));
	//Assert the size of the directional light array is less than the total
	//allocated memory in the shader.
	assert(dLights.size() <= 2);
	for (unsigned int i = 0; i < dLights.size(); ++i)
	{
		dLights[i].loadData(constData.pShaderCBuffer.directionalLights[i]);
	}
	assert(sLights.size() <= 9);
	for (unsigned int i = 0; i < sLights.size(); ++i)
	{
		sLights[i].loadData(constData.pShaderCBuffer.spotLights[i]);
	}
	assert(pLights.size() <= 1);
	for (unsigned int i = 0; i < pLights.size(); ++i)
	{
		pLights[i].loadData(constData.pShaderCBuffer.pointLights[i]);
	}

	if (currentGameState == gamePlay)
	{
		spriteBatch->Begin(SpriteSortMode_Deferred, m_states->NonPremultiplied());
		backGround->Draw(spriteBatch.get(), Vector2(0.0f, 0.0f));

		if (paused)
		{
			//temp rendering for pause
			Vector2 textOrigin = DirectX::XMVectorDivide(spriteFont->MeasureString(L"Paused"), DirectX::XMVectorSet(2.0f, 2.0f, 0.0f, 0.0f));
			spriteFont->DrawString(spriteBatch.get(), L"Paused", XMFLOAT2(windowWidth * 0.5f, windowHeight * 0.5f), DirectX::Colors::Black, 0.0f, textOrigin, 2.5f);
		}
		spriteBatch->End();
		deviceContext->OMSetDepthStencilState(NULL, 0);

		// Draw snake related stuff
		floor.Draw(constData);
		floorBg->Draw(constData);
		snake->Draw(constData);
		

		// Draw particles 
		particleManager->Draw(
			deltaTime,
			totalTime,
			constData.vShaderCBuffer.view,
			constData.vShaderCBuffer.projection,
			deviceContext);
	}
	else
	{
		//start rendering sprites/text
		spriteBatch->Begin(SpriteSortMode_Deferred, m_states->NonPremultiplied());
		backGround->Draw(spriteBatch.get(), Vector2(0.0f, 0.0f));

		if (currentGameState == mainMenu)
		{
			startBtn->Draw(spriteBatch);
			exitBtn->Draw(spriteBatch);
		}

		//stop rendering sprites/text
		spriteBatch->End();
	}

	// Present the buffer once per frame, always at the end of the draw method
	HR(swapChain->Present(0, 0));
}

#pragma endregion

#pragma region Mouse Input

// --------------------------------------------------------
// Helper method for mouse clicking.  We get this information
// from the OS-level messages anyway, so these helpers have
// been created to provide basic mouse input if you want it.
//
// Feel free to add code to this method
// --------------------------------------------------------
void MyDemoGame::OnMouseDown(WPARAM btnState, int x, int y)
{
	mouseDown = true;
	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;

	// Caputure the mouse so we keep getting mouse move
	// events even if the mouse leaves the window.  we'll be
	// releasing the capture once a mouse button is released
	SetCapture(hMainWnd);
}

// --------------------------------------------------------
// Helper method for mouse release
//
// Feel free to add code to this method
// --------------------------------------------------------
void MyDemoGame::OnMouseUp(WPARAM btnState, int x, int y)
{
	mouseDown = false;
	// We don't care about the tracking the cursor outside
	// the window anymore (we're not dragging if the mouse is up)
	ReleaseCapture();
}

// --------------------------------------------------------
// Helper method for mouse movement.  We only get this message
// if the mouse is currently over the window, or if we're 
// currently capturing the mouse.
//
// Feel free to add code to this method
// --------------------------------------------------------
void MyDemoGame::OnMouseMove(WPARAM btnState, int x, int y)
{
	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;
}
#pragma endregion
