#pragma once
#include <DirectXMath.h>
#include <d3d11.h>

class Transform
{
public:
	Transform();
	Transform(const DirectX::XMFLOAT3& position);
	Transform(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT4& rotation, const DirectX::XMFLOAT3& scale);
	~Transform();

	void SetPosition(const DirectX::XMFLOAT3& p);
	void SetScale(const DirectX::XMFLOAT3& s);
	void SetRotation(const DirectX::XMFLOAT4& r);
	void SetRotation(const DirectX::XMVECTOR& r);

	void Translate(const DirectX::XMFLOAT3& offset);
	void Translate(const DirectX::XMVECTOR& offset);
	void Rotate(const DirectX::XMVECTOR& offset);
	void Rotate(const DirectX::XMFLOAT4& offset);

	const DirectX::XMFLOAT3& GetPosition() const;
	const DirectX::XMFLOAT3& GetScale() const;
	const DirectX::XMFLOAT4& GetRotation() const;
	DirectX::XMFLOAT4X4 GetMatrix() const;

	DirectX::XMVECTOR GetForwardVector() const;
	DirectX::XMVECTOR GetRightVector() const;
	DirectX::XMVECTOR GetUpVector() const;

private:
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT4 rotation;			
	DirectX::XMFLOAT3 worldForward = DirectX::XMFLOAT3(0, 0, 1.0f);
	DirectX::XMFLOAT3 worldRight = DirectX::XMFLOAT3(1.0f, 0, 0);
	DirectX::XMFLOAT3 worldUp = DirectX::XMFLOAT3(0, 1.0f, 0);
	
};
