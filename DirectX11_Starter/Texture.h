#pragma once
#include <d3d11.h>
#include <wrl\client.h>

struct Texture
{
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> svr;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
};
