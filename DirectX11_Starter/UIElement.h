#pragma once
#include <d3d11.h>
#include <SimpleMath.h>
#include <SpriteBatch.h>
#include <wrl\client.h>
#include <memory>
#include "Texture.h"
using namespace DirectX::SimpleMath;

class UIElement
{
public:
	UIElement(std::shared_ptr<Texture> texture, Vector2 position, float rotation, Vector2 scale);
	virtual ~UIElement();

	virtual void Draw(std::unique_ptr<DirectX::SpriteBatch> const &spriteBatch);

	Vector2 GetPosition() const;
	Vector2 GetScale() const;
	float GetRotation() const;

	void SetPosition(Vector2 position);
	void SetRotation(float rotation);
	void SetScale(Vector2 scale);

	void Resize();

protected:
	
	std::shared_ptr<Texture> texture;
	Vector2 origin;
	Vector2 position;
	Vector2 scale;
	float rotation;
	RECT bounds;
};

