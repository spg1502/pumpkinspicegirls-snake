#pragma once

#include <DirectXMath.h>

// Any variable data for a Basic Particle Sys that can change sys to sys 
struct BaseSystemPatricleData
{
	// Physics data 
	DirectX::XMFLOAT3 startPosition;
	DirectX::XMFLOAT3 startVelocity;
	DirectX::XMFLOAT3 constAcceleration;

	// Color data 
	DirectX::XMFLOAT4 startColor;
	DirectX::XMFLOAT4 midColor;
	DirectX::XMFLOAT4 endColor;

	// Size data 
	DirectX::XMFLOAT3 smeSizes;

	// Life data 
	float ageToSpawn;
	float maxLifespan; 
	float numParticles; 

	// Spawn data 
	float xzRandomFactor; 
};