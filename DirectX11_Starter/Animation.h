#pragma once
#include <SpriteBatch.h>
#include <wrl\client.h>
#include <memory>
#include <SimpleMath.h>
#include "Texture.h"

class Animation
{
public:
	Animation();

	Animation(const DirectX::SimpleMath::Vector2& origin, float rotation, float scale, float depth);

	void Load(std::shared_ptr<Texture> texture, int frameCount, int framesPerSecond, int frameWidth, int frameHeight);
	void Update(float elapsed);
	void Draw(DirectX::SpriteBatch* batch, const DirectX::SimpleMath::Vector2 &screenPos) const;
	void Draw(DirectX::SpriteBatch* batch, int row, int column, const DirectX::SimpleMath::Vector2 &screenPos) const;
	void Reset();
	void Stop();

	void Play() { paused = false; }
	void Paused() { paused = true; }
	bool IsPaused() const { return paused; }

private:
	bool paused;
	int frameCount;
	int frame;
	int row;
	int column;
	int frameWidth;
	int frameHeight;
	int textureWidth;
	int textureHeight;
	float timePerFrame;
	float totalElapsed;
	float depth;
	float rotation;
	DirectX::SimpleMath::Vector2 origin;
	DirectX::SimpleMath::Vector2 scale;
	//Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> texture;
	std::shared_ptr<Texture> texture;
};

