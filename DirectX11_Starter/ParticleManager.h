#pragma once

#include "ParticleSysInput.h"
#include "ConfettiExplosion.h"

// Loads and stores any reusable data for particle effects 
// Will create and run particle system for selected duration 
// Shuts down systems when effect is over 
class ParticleManager
{
public:
	ParticleManager(
		ID3D11Device* device,
		ID3D11DeviceContext* deviceContext,
		wchar_t* confettiTexturePath);
	~ParticleManager();

	// Confetti Explosion - position 
	void SpawnConfettiEffect(DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 foodPos, ID3D11Device* device, ID3D11DeviceContext* deviceContext);

	// Draw particle effects 
	void Draw(
		float deltaTime,
		float totalTime,
		const DirectX::XMFLOAT4X4& view,
		const DirectX::XMFLOAT4X4& projection,
		ID3D11DeviceContext* deviceContext);

	// Clean up any active particle effects 
	void CleanUp(); 

private:

	// Hold Active Particle effect 
	std::vector<ConfettiExplosion*> activeConfettiEffect; 

	// ----------------------------------------------------------------------
	// Shared data between systems 

	// Blend / Depth states 
	ID3D11BlendState* particleBlendState;
	ID3D11DepthStencilState* particleDepthState;

	// Random number variables  
	ID3D11Texture1D* randomTexture;
	ID3D11ShaderResourceView* randomDataSRV;

	// Basic texture sampler 
	ID3D11SamplerState* particleSampler;

	// Shared data initilization
	void CreateRandomTexture(ID3D11Device* device);
	void CreateBlendDepthStates(ID3D11Device* device);


	// ----------------------------------------------------------------------
	// Confetti explosion - Data we can load and save once 

	// Basic vars 
	BaseSystemPatricleData defaultConfettiVariables; 

	// Particle shaders 
	SimpleVertexShader* particleVS;
	SimplePixelShader* particlePS;
	SimpleGeometryShader* particleGS;

	// Patricle spawner shaders 
	SimpleVertexShader* particleSpawnVS;
	SimpleGeometryShader* particleSpawnGS;

	// Actual particle texture 
	ID3D11ShaderResourceView* confettiTexture;

	// Initilize all the nessary data for confetti, used in the constructor 
	void InitConfettiData(ID3D11Device* device, ID3D11DeviceContext* deviceContext, wchar_t* texturePath);

};

