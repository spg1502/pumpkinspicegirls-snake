#include "Snake.h"

namespace
{
	const float speedMultiplier = 2.0f;
	const float distanceBetweenCups = 0.85f;
}

Snake::Snake()
{

}

Snake::Snake(
	Mesh* headMesh,
	Mesh* bodyMesh,
	Mesh* foodMesh,
	Material* headMat,
	Material* bodyMat,
	Material* foodMat) :
	bodyMesh(bodyMesh),
	bodyMat(bodyMat),
	bodyScale(.5,.5,.5),
	moveAmount(8,0,8),
	deltaForwardAngle(Constants::PI*1.75),
	distanceBetweenPlacing(distanceBetweenCups),
	head(
		headMesh, 
		headMat, 
		DirectX::SimpleMath::Vector3::Zero, 
		DirectX::SimpleMath::Quaternion::Identity, 
		DirectX::SimpleMath::Vector3(0.75f)),
	food(foodMesh, foodMat, AREA_WIDTH, AREA_LENGTH)
{

	// Create the head of the snake  
	headForwardAsAngle = INIT_MOVE_DIRECTION;

	// Create some initial body pieces 
	for (int i = 0; i <= INIT_BODY_SEGMENTS; ++i)
	{
		// Create and set scale of mesh 
		bodyComponents.push_back(
			Entity(
				bodyMesh, 
				bodyMat, 
				DirectX::SimpleMath::Vector3::Zero, 
				DirectX::SimpleMath::Quaternion::Identity, 
				DirectX::SimpleMath::Vector3(0.5f)));
		// Set position so they dont collide at the start
		DirectX::XMFLOAT3 pos(0, 0, -i*.5);
		bodyComponents.back().getTransform().SetPosition(pos);

		// Update the transfroms rotation 
		// Gonna need help with how to use the transform.rotate(), currentally is scaling 
		bodyComponents.back().getTransform().Rotate(HeadAngleToForward());
	}
}

Snake::~Snake()
{

}



size_t Snake::GetLength() const
{
	return bodyComponents.size() + 1;
}
DirectX::XMFLOAT3 Snake::GetHeadPos() const
{
	return head.getTransform().GetPosition(); 
}
Entity* Snake::getHead()
{
	return &head;
}
float Snake::GetAngularVelocity() const { return angularVelocity; }

// Returns revelent info as an int
// 0 - Alive (nothing special)
// 1 - Dead 
// 2 - Picked up food this frame 
int Snake::Update(float deltaTime)
{
	// Check key input 
	KeyInput(deltaTime);

	// Head + body movement 
	Move(deltaTime);

	// Check for eating food 
	DirectX::XMFLOAT3 p = head.getTransform().GetPosition();
	if (food.DidCollide(head.getTransform().GetScale().x/2, Vector3(p.x,p.y,p.z)))
	{
		// Add to the body when you do
		switch (food.getType())
		{
		case PelletType::Speed:
			speedTime = 2.0f;
			break;
		case PelletType::Standard:
			for (int i = 0; i < NUM_BODY_ADD_PLUS_ONE; ++i)
			{
				NewComponentAtEnd();
			}
			break;
		}

		//After we collide, change the location & re-roll type
		food.Reset();

		// Return to inform the game manager you ate food 
		return 2;
	}

	// 1 = dead 
	if (IsDead()) return 1;

	// Defualt return is 0 (nothing) 
	return 0;
}

void Snake::KeyInput(float deltaTime)
{
	angularVelocity = 0;
	// Keys for movement -> change the forward of the head
	if (GetAsyncKeyState('N') & 0x8000)
	{
		angularVelocity = -deltaForwardAngle;
		headForwardAsAngle -= deltaForwardAngle * deltaTime;
	}
	if (GetAsyncKeyState('M') & 0x8000)
	{
		angularVelocity = deltaForwardAngle;
		headForwardAsAngle += deltaForwardAngle * deltaTime;
	}
}

void Snake::Draw(ShaderConstants& shaderConstData)
{
	// Call the draw function of each body component 
	for each(Entity e in bodyComponents)
	{
		e.Draw(shaderConstData);
	}
	head.Draw(shaderConstData);

	// Draw the food 
	food.Draw(shaderConstData);
}

Vector3 Snake::HeadAngleToForward()
{
	Vector3 newFor = Vector3(sin(headForwardAsAngle), 0, cos(headForwardAsAngle));
	newFor.Normalize();
	return(newFor);
}

void Snake::NewComponentAtEnd()
{
	// Add new entity to the dequeue 
	bodyComponents.push_back(
		Entity(
			bodyMesh, 
			bodyMat, 
			DirectX::SimpleMath::Vector3::Zero, 
			DirectX::SimpleMath::Quaternion::Identity, 
			DirectX::SimpleMath::Vector3(0.5f)));

	// Only need to get the back index once 
	size_t backIndex = bodyComponents.size() - 1;

	// Place the part / set values 
	DirectX::XMFLOAT3 p = bodyComponents[backIndex-1].getTransform().GetPosition();
	bodyComponents[backIndex].getTransform().SetPosition(p);

	// Update the transfroms rotation 
	// Gonna need help with how to use the transform.rotate(), currentally is scaling 
	bodyComponents[backIndex].getTransform().Rotate(HeadAngleToForward());
}

void Snake::NewComponentAtHead()
{
	// Place at heads positon
	DirectX::XMFLOAT3 pos = head.getTransform().GetPosition();
	Vector3 newPos = Vector3(pos.x, 0, pos.z);
	bodyComponents.push_front(
		Entity(
			bodyMesh, 
			bodyMat, 
			newPos, 
			DirectX::SimpleMath::Quaternion::Identity, 
			DirectX::SimpleMath::Vector3(0.5f)));

	// Update the transfroms rotation 
	// Gonna need help with how to use the transform.rotate(), currentally is scaling 
	bodyComponents.front().getTransform().Rotate(HeadAngleToForward());
}

void Snake::Move(float deltaTime)
{
	// HEAD MOVEMENT 
	// Head always moving, controlled by changing the forward 
	Vector3 displacement = moveAmount * deltaTime * HeadAngleToForward();
	if (speedTime >= 0.0f)
	{
		displacement *= speedMultiplier;
		speedTime -= deltaTime;
	}
	DirectX::XMFLOAT3 p = head.getTransform().GetPosition(); 
	Vector3 newPos = Vector3(p.x, HEAD_Y_DISPLACEMENT, p.z) + displacement;
	head.getTransform().SetPosition(newPos); 
	distanceBetweenPlacing -= displacement.Length();

	//Rotate head to face the correct direction
	DirectX::XMFLOAT4 newRotation;
	DirectX::XMFLOAT3 upDirection{ 0, 1.0f, 0 };
	auto upVec = DirectX::XMLoadFloat3(&upDirection);
	DirectX::XMVECTOR rotationQuat = DirectX::XMQuaternionRotationAxis(upVec, headForwardAsAngle);
	DirectX::XMStoreFloat4(&newRotation, rotationQuat);
	head.getTransform().SetRotation(newRotation);

	// BODY MOVEMENT
	// Add at the head, remove from the back 
	// Needs to be synced up with the speed 
	if (distanceBetweenPlacing <= 0.0f)
	{
		NewComponentAtHead();
		bodyComponents.pop_back();

		distanceBetweenPlacing = distanceBetweenCups;
	}

}

bool Snake::IsDead()
{
	// Get variables needed
	float headRadius = head.getTransform().GetScale().x * 0.5f;
	float bodyRadius = bodyComponents[0].getTransform().GetScale().x * 0.5f;
	DirectX::XMFLOAT3 p = head.getTransform().GetPosition();
	Vector3 headPos(p.x, p.y, p.z);

	// Check the head aganist all body components 
	// Can skip the first few, cant physically reach them
	for (int i = 3; i < bodyComponents.size(); ++i)
	{
		// If collide, break out and true 
		DirectX::XMFLOAT3 bp = bodyComponents[i].getTransform().GetPosition();
		if (CollisionDetection::CircleCollision(headRadius, headPos, bodyRadius, Vector3(bp.x, bp.y, bp.z)))
		{
			return true;
		}
	}

	return false;
}


void Snake::Reset()
{
	// Clear body
	bodyComponents.clear(); 

	// Reset head position 
	head.getTransform().SetPosition(DirectX::XMFLOAT3(0, 0, 0));
	headForwardAsAngle = INIT_MOVE_DIRECTION;

	// Create body 
	for (int i = 0; i <= INIT_BODY_SEGMENTS; ++i)
	{
		// Create and set scale of mesh 
		bodyComponents.push_back(
			Entity(
				bodyMesh,
				bodyMat,
				DirectX::SimpleMath::Vector3::Zero,
				DirectX::SimpleMath::Quaternion::Identity,
				DirectX::SimpleMath::Vector3(0.5f)));
		// Set position so they dont collide at the start
		DirectX::XMFLOAT3 pos(0, 0, -i*.5);
		bodyComponents.back().getTransform().SetPosition(pos);
	}
}

DirectX::SimpleMath::Vector3 Snake::GetFoodPos() const
{
	return food.GetPos(); 
}