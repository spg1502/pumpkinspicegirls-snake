#include "UIElement.h"

UIElement::UIElement(std::shared_ptr<Texture> tex, Vector2 pos, float rot, Vector2 sca) :
	texture(tex), position(pos), rotation(rot), scale(sca)
{
	D3D11_TEXTURE2D_DESC texDesc = {};
	texture->texture->GetDesc(&texDesc);
	origin = Vector2(texDesc.Width / 2.0f, texDesc.Height / 2.0f);
	Resize();
}

void UIElement::Draw(std::unique_ptr<DirectX::SpriteBatch> const &sb)
{
	sb->Draw(texture->svr.Get(), position, nullptr, DirectX::Colors::White, rotation, origin, scale);
}

UIElement::~UIElement()
{
}

void UIElement::Resize()
{
	SetRect(&bounds, position.x - (origin.x * scale.x), position.y - (origin.y * scale.y), position.x + (origin.x * scale.x), position.y + (origin.y * scale.y));
}

#pragma region Getters

Vector2 UIElement::GetPosition() const
{
	return position;
}

Vector2 UIElement::GetScale() const
{
	return scale;
}

float UIElement::GetRotation() const
{
	return rotation;
}

#pragma endregion

#pragma region Setters

void UIElement::SetPosition(Vector2 pos)
{
	position = pos;
	Resize();
}

void UIElement::SetRotation(float rot)
{
	rotation = rot;
}

void UIElement::SetScale(Vector2 sca)
{
	scale = sca;
	Resize();
}

#pragma endregion


