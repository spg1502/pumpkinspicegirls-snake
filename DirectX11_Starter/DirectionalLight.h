#pragma once
#include "Entity.h"
#include "ShaderConstants.h"
#include <DirectXMath.h>

//This defines the data required for a directional light
//The last value in Direction (the W in xyzw) is never used, so set it to 0
//Direction was made an XMFLOAT4 instead of an XMFLOAT3 for data-packing reasons, don't mess with it unless you have good reason to and know what you're doing

class DirectionalLight : public Entity
{
public:
	DirectionalLight();
	DirectionalLight(
		DirectX::SimpleMath::Color pAmbientColor,
		DirectX::SimpleMath::Color pDiffuseColor,
		DirectX::SimpleMath::Quaternion pRotation);
	DirectionalLight(
		const DirectX::SimpleMath::Color& pAmbientColor,
		const DirectX::SimpleMath::Color& pDiffuseColor,
		const DirectX::SimpleMath::Vector3& pDirection);
	DirectionalLight(Entity e);
	~DirectionalLight();
	void setAmbientColor(const DirectX::SimpleMath::Color& pAmbientColor);
	void setDiffuseColor(const DirectX::SimpleMath::Color& pDiffuseColor);
	void setDirection(const DirectX::SimpleMath::Quaternion& pDirection);

	void loadData(PixelShaderCBuffer::DirectionalLight& dataContainer);

	const DirectX::SimpleMath::Color& getAmbientColor() const;
	const DirectX::SimpleMath::Color& getDiffuseColor() const;
	DirectX::SimpleMath::Vector3 getDirection() const;

	void Update(float totalTime);

private:
	DirectX::SimpleMath::Color ambientColor;
	DirectX::SimpleMath::Color diffuseColor;
};