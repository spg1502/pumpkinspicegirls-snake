#pragma once
#include "SimpleShader.h"
#include <DirectXMath.h>
#include <WICTextureLoader.h>

struct ShaderConstants;

class Material
{
public:

	Material(SimpleVertexShader* vertexShader, SimplePixelShader* pixelShader, ID3D11Device* device);
	~Material();

	// Access to shaders 
	SimpleVertexShader* GetVertexShader() const;
	SimplePixelShader* GetPixelShader() const;

	// Load any texture maps 
	void LoadDiffuseMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath);
	void LoadSpecularMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath);
	void LoadNormalMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath);

	const ID3D11ShaderResourceView* getDiffuseMap() const;
	ID3D11ShaderResourceView* getDiffuseMap();
	const ID3D11ShaderResourceView* getSpecularMap() const;
	ID3D11ShaderResourceView* getSpecularMap();
	const ID3D11ShaderResourceView* getNormalMap() const;
	ID3D11ShaderResourceView* getNormalMap();

	void setDiffuseMap(ID3D11ShaderResourceView* newDiffuseMap);
	void setSpecularMap(ID3D11ShaderResourceView* newSpecularMap);
	void setNormalMap(ID3D11ShaderResourceView* newNormalMap);



	// Sending data to shaders
	// If material is storing the shaders and any texture data, may as well 
	// use it to do the "Set...", aka. sending data to the shaders 
	// Will need to grab the transfrom matrix from the entity 
	void PrepShaders(ShaderConstants& shaderConstData);
	

private:

	// Shaders to use for this material 
	SimpleVertexShader* vertexShader;
	SimplePixelShader* pixelShader;

	// Texture maps to use 
	ID3D11ShaderResourceView* diffuseMap; 
	ID3D11ShaderResourceView* specularMap; 
	ID3D11ShaderResourceView* normalMap; 

	// Texture sampler, can be used for all maps 
	ID3D11SamplerState* sampler; 
};

