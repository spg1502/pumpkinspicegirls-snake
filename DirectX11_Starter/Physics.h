#pragma once
#include <SimpleMath.h>

namespace CollisionDetection
{
	using namespace DirectX::SimpleMath;

	inline bool CircleCollision(float r1, const Vector3& p1, float r2, const Vector3& p2)
	{
		float distSq = (p1.x - p2.x)*(p1.x - p2.x) + (p1.z - p2.z)*(p1.z - p2.z);
		return ((r1 + r2) * (r1 + r2) > distSq);
	}

}