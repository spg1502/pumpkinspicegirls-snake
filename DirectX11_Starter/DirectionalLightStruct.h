#pragma once
#include <DirectXMath.h>

//This describes the data that goes into a directional light
// The format of this data must match exactly with the struct in PixelShader in order to accurately pass in information about lights to the pixelShader
// NOTE: the last value in Direction (the W in xyzw) is never used, so set it to 0
// Direction was made an XMFLOAT4 instead of an XMFLOAT3 for data-packing reasons, don't mess with it unless you have good reason to and know what you're doing

struct DirectionalLightStruct {
	DirectX::XMFLOAT4 AmbientColor;
	DirectX::XMFLOAT4 DiffuseColor;
	DirectX::XMFLOAT4 Direction;
};