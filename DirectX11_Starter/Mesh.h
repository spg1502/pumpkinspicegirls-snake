#pragma once

#include <DirectXMath.h>
#include <d3d11.h>
#include "DirectXGameCore.h"
#include "Vertex.h"
#include "SimpleShader.h"
#include <SimpleMath.h>

class Mesh
{
public:
	//Parameters for manual mesh construction
	// -pVertexArray - an array of vertices
	// -pVerticesInVertexArray - an int with a value equal to the number of elements in pVertexArray
	// -pIndexArray - an array that describes the order in which vertices should be read in to create objects with normals in the correct direction
	// -pIndicesInIndexArray - an int with a value equal to the number of elements in pIndexArray
	// -pBufferCreator, pDeviceContext - ID3D11 objects used in object creation
	// -pNumVerts - the number of vertices to be drawn
	// -pOffsetToFirstIndex - the offset from the start in memory that the object points to to the start of the first piece of data, usually 0
	// -pOffsetStepSize - the "size" of each vertex object, this tells the program how far to look ahead when reading data from one vertex to the next
	Mesh(Vertex* pVertexArray, int pVerticesInVertexArray, unsigned int* pIndexArray, int pIndicesInIndexArray, ID3D11Device* pBufferCreator, ID3D11DeviceContext* pDeviceContext,
		int pNumVerts, int pOffsetToFirstIndex, int pOffsetStepSize);
	//Parameters for file-based mesh construction
	// -filename - the name of a .obj file in the MainRepoFolder/debug/ folder
	// -pBufferCreator, pDeviceContext - ID3D11 objects used in object creation
	Mesh(char* filename, ID3D11Device* pBufferCreator, ID3D11DeviceContext* pDeviceContext);
	~Mesh();

	void DrawMesh();

	DirectX::XMFLOAT3 GetExtents() const;
	DirectX::XMFLOAT3 MinPoint() const;
	DirectX::XMFLOAT3 MaxPoint() const;

private:
	// Variables for the buffers
	Vertex* vertexArray;
	int verticesInVertexArray;
	unsigned int* indexArray;
	int indicesInIndexArray;
	ID3D11Device* bufferCreator;
	ID3D11DeviceContext* deviceContext;
	ID3D11Buffer* vertexBufferPointer;
	ID3D11Buffer* indexBufferPointer;

	// Drawing variables
	int numVerts;
	int offsetToFirstIndex;
	int offsetStepSize;

	// Wrappers for DirectX shaders to provide simplified functionality
	SimpleVertexShader* vertexShader;
	SimplePixelShader* pixelShader;

	int indexBufferIndices;

	void CreateBuffers();
	void SetBuffers();
	void DrawIndexedMesh();

	ID3D11Buffer* GetVertexBuffer();
	ID3D11Buffer* GetIndexBuffer();
	int GetIndexCount();

	DirectX::XMFLOAT3 minPoint;
	DirectX::XMFLOAT3 maxPoint;
	DirectX::XMFLOAT3 extents;
};

