#pragma once
#include "Entity.h"

class QuatCamera :
	public Entity
{
public:
	QuatCamera(float aspectRatio);
	QuatCamera(float aspectRatio, Entity* entityToFollow, float height);
	~QuatCamera();

	DirectX::XMFLOAT4X4 GetViewMatrix();
	DirectX::XMFLOAT4X4 GetProjectionMatrix();

	void SetEntityToFollow(Entity*);

	//Change projection matrix parameters
	void SetNearClip(float dist);
	void SetFarClip(float dist);
	void SetAspectRatio(float r);
	void SetFOV(float fov);

	//Movement controls
	void MoveForward(float dist);
	void MoveRight(float dist);
	void MoveUp(float dist);
	void ToggleFPSMode();
	void SetMoveSpeed(float speed);

	//Mouse look controls
	void RotateY(float angle);
	void RotateX(float angle);

	void Update(float dt);

private:
	//Projection variables
	float FOV = .25 * 3.14159265;			//Camera comes with reasonable default values which can be changed at any time
	float nearPlaneDist = .1f;
	float farPlaneDist = 100.0f;
	float aspectRatio;

	Entity* entityToFollow;
	float followHeight = 20.0f;
	float pitch;
	float yaw;
	float movementSpeed = 5.0f;

	DirectX::XMFLOAT4X4 viewMatrix;
	DirectX::XMFLOAT4X4 projectionMatrix;
	DirectX::XMFLOAT3 upAxis;
	DirectX::XMFLOAT3 rightAxis;

	bool viewMatrixIsDirty;
	bool fpsMode;

	void createProjectionMatrix();
	void createViewMatrix();
};

