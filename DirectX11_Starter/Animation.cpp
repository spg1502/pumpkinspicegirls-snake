#include "Animation.h"
using namespace DirectX::SimpleMath;


Animation::Animation() :
	paused(false),
	frame(0),
	frameCount(0),
	textureWidth(0),
	textureHeight(0),
	timePerFrame(0.f),
	totalElapsed(0.f),
	rotation(0.f),
	scale(1.f, 1.f),
	depth(0.f),
	origin(0.f, 0.f)
{
}

Animation::Animation(const Vector2& origin,
	float rotation,
	float scale,
	float depth) :
	paused(false),
	frame(0),
	frameCount(0),
	textureWidth(0),
	textureHeight(0),
	timePerFrame(0.f),
	totalElapsed(0.f),
	rotation(rotation),
	scale(scale, scale),
	depth(depth),
	origin(origin)
{
}

void Animation::Load(std::shared_ptr<Texture> _texture, int _frameCount, int _framesPerSecond, int _frameWidth, int _frameHeight)
{
	paused = false;
	frame = 0;
	frameWidth = _frameWidth;
	frameHeight = _frameHeight;
	frameCount = _frameCount;
	timePerFrame = 1.0f / float(_framesPerSecond);
	totalElapsed = 0.0f;
	texture = _texture;

	if (texture)
	{
		D3D11_TEXTURE2D_DESC desc;
		texture->texture->GetDesc(&desc);
		textureWidth = int(desc.Width);
		textureHeight = int(desc.Height);
	}
}

void Animation::Update(float elapsed)
{

	if (paused)
		return;

	totalElapsed += elapsed;

	if (totalElapsed > timePerFrame)
	{
		++frame;
		++row;

		if (row * frameWidth >= textureWidth)
		{
			row = 0;
			column++;
		}

		if (frame >= frameCount)
		{
			row = 0;
			column = 0;
			frame = 0;
		}

		totalElapsed -= timePerFrame;
	}

}

void Animation::Draw(DirectX::SpriteBatch* batch, const Vector2& screenPos) const
{
	Draw(batch, row, column, screenPos);
}

void Animation::Draw(DirectX::SpriteBatch* batch, int row, int column, const Vector2& screenPos) const
{
	RECT sourceRect;
	sourceRect.left = frameWidth * row;
	sourceRect.top = frameHeight * column;
	sourceRect.right = sourceRect.left + frameWidth;
	sourceRect.bottom = sourceRect.top + frameHeight;

	batch->Draw(texture->svr.Get(), screenPos, &sourceRect, DirectX::Colors::White,
		rotation, origin, scale, DirectX::SpriteEffects_None, depth);
}

void Animation::Reset()
{
	frame = 0;
	totalElapsed = 0.f;
}

void Animation::Stop()
{
	paused = true;
	frame = 0;
	totalElapsed = 0.f;
}