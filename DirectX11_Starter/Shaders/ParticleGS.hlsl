
// Data from outside the shader 
cbuffer externalData : register(b0)
{
	matrix world; 
	matrix view; 
	matrix projection; 
}

// Input data from the VS
struct VStoGS
{
	int type			: TEXCOORD0;
	float3 position		: POSITION;
	float4 color		: COLOR;
	float size			: TEXCOORD1;
};

// Output data to the PS
struct GStoPS
{
	float4 position : SV_POSITION;
	float4 color	: COLOR;
	float2 uv		: TEXCOORD;
};


[maxvertexcount(4)]
void main( point VStoGS input[1], inout TriangleStream< GStoPS > outStream)
{
	// Skip the root particle
	if (input[0].type = 0) return;

	GStoPS output; 

	// Offset from the center point for triangles 
	float2 offsets[4];
	offsets[0] = float2(-0.1f, -0.1f);
	offsets[1] = float2(-0.1f, +0.1f);
	offsets[2] = float2(+0.1f, -0.1f);
	offsets[3] = float2(+0.1f, +0.1f);

	// Calc the world view proj
	matrix wvp = mul(mul(world, view), projection); 

	[unroll]
	for (int o = 0; o < 4; o++)
	{
		// Add a vertex to the steam 
		output.position = mul(float4(input[0].position, 1.0f), wvp);

		// Depth 
		float depthChange = output.position.z / output.position.w;

		// Adjust based on depth 
		output.position.xy += offsets[o] * depthChange * input[0].size; 
		output.color = input[0].color; 
		output.uv = saturate(offsets[o] * 10); 

		// Done
		outStream.Append(output);
	}
}