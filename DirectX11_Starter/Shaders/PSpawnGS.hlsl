
#define TYPE_ROOT 0 
#define TYPE_PARTICLE 1

// Input from VS
struct VStoGS
{
	int type			: TEXCOORD0;
	float age			: TEXCOORD1;
	float3 startPos		: POSITION;
	float3 startVel		: TEXCOORD2;
	float4 startColor	: COLOR0;
	float4 midColor		: COLOR1;
	float4 endColor		: COLOR2;
	float3 sizes		: TEXCOORD3;
};

// Outside data 
cbuffer externalData : register(b0)
{
	float dt; 
	float ageToSpawn;
	float maxLifetime;
	float totalTime;
	float xzRandomFactor;
}

texture1D randomTexture : register (t0);
sampler randomSampler : register (s0);

[maxvertexcount(2)]
void main(point VStoGS input[1], inout PointStream<VStoGS> outStream)
{
	// Increment age 
	input[0].age += dt;

	// Check if root particle
	if (input[0].type == TYPE_ROOT)
	{
		// Time for a new particle?
		if(input[0].age >= ageToSpawn)
		{
			// Reset root 
			input[0].age = 0;

			// Make a copy to emit 
			VStoGS emit;
			emit.type = TYPE_PARTICLE;
			emit.age = 0;
			emit.startPos = input[0].startPos;
			emit.startVel = input[0].startVel;
			emit.startColor = input[0].startColor;
			emit.midColor = input[0].midColor;
			emit.endColor = input[0].endColor;
			emit.sizes = input[0].sizes;

			// Alter vales for start 
			float4 random = randomTexture.SampleLevel(randomSampler, totalTime * 10, 0);
			emit.startPos += random.xyz * 0.3f; 
			emit.startVel.x += random.w * xzRandomFactor;
			emit.startVel.y += random.y * 0.1f;
			emit.startVel.z += random.x * xzRandomFactor;

			// Add it to the out steam
			outStream.Append(emit);
		}

		// Always keep the root particle 
		outStream.Append(input[0]);
	}
	else if (input[0].age < maxLifetime)
	{
		// Keep particles if their age is ok
		outStream.Append(input[0]);
	}
}