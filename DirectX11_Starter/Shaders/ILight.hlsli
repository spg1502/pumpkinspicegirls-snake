#ifndef __ILIGHT__
#define __ILIGHT__

interface ILight 
{
	float4 ComputeAmbient(float3 worldPos, float3 normal);
	float4 ComputeDiffuse(float3 worldPos, float3 normal);

	float4 ComputeColor(
		float4 surfaceAmbient, 
		float4 surfaceDiffuse,
		float3 worldPos, 
		float3 normal);
};

#endif