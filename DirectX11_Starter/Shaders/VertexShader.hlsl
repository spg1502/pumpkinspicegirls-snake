
// Constant Buffer
//	All data we need to pass into the vertex shader from mydemogame.cpp
cbuffer externalData : register(b0)
{
	matrix world;
	matrix view;
	matrix projection;
};

// Struct representing a single vertex worth of data
// - This should match the vertex definition in our C++ code
struct VertexShaderInput
{ 
	// Data type
	//  |
	//  |   Name          Semantic
	//  |    |                |
	//  v    v                v
	float3 position		: POSITION;     // XYZ position
	float3 normal		: NORMAL;       // Normal vector
	float2 uv			: TEXCOORD;		// UV texture coordinates
};

// Struct representing the data we're sending down the pipeline
// - Should match our pixel shader's input (hence the name: Vertex to Pixel)
// - At a minimum, we need a piece of data defined tagged as SV_POSITION
struct VertexToPixel
{
	// Data type
	//  |
	//  |   Name          Semantic
	//  |    |                |
	//  v    v                v
	float4 position		: SV_POSITION;	// XYZW position (System Value Position)
	float3 normal		: NORMAL;		// The normal that has been modified to be rotated along with the world matrix
	float3 worldPos		: TEXCOORD0;	// This is an arbitrary Semantic that needs to match the Semantic in the pixel's input, This just gives us a semantic that lets us pass arbitrary data about the position of the vertex
	float2 uv			: TEXCOORD1;	// Again, an arbitrary Semantig used to pass arbitrary data through
};

// --------------------------------------------------------
// The entry point (main method) for our vertex shader
// 
// - Input is exactly one vertex worth of data (defined by our struct)
// - Output is a single struct of data to pass down the pipeline
// - Named "main" because that's the default the shader compiler looks for
// --------------------------------------------------------
VertexToPixel main( VertexShaderInput input )
{
	// Set up output struct
	VertexToPixel output;

	// First we multiply world view and proj together to get a single matrix which represents
	// all of those transformations (world to view to projection space)
	matrix worldViewProj = mul(mul(world, view), projection);

	// Then we convert our 3-component position vector to a 4-component vector
	// and multiply it by our final 4x4 matrix.
	//
	// The result is essentially the position (XY) of the vertex on our 2D 
	// screen and the distance (Z) from the camera (the "depth" of the pixel)
	output.position = mul(float4(input.position, 1.0f), worldViewProj);

	//Adding the normal to the output, so that the pixel shader can interact with the lights
	//Take into account rotation (but not translation) so that as the object rotates, the lighting reflects that
	output.normal = mul(input.normal, (float3x3)world);

	// The world space position of the vertex
	//  float4(input.position, 1) turns the float 3 position into a float 4x4 with the same data for all rows, and a 1 as the W value
	//	.xyz at the end indicates that we only want the XYZ values from the resulting 4x4 matrix
	output.worldPos = mul(float4(input.position, 1), world).xyz;

	// The UV coordinate for texture sampling
	output.uv = float2(input.uv.x, 1.0f - input.uv.y);

	// Whatever we return will make its way through the pipeline to the
	// next programmable stage we're using (the pixel shader for now)
	return output;
}