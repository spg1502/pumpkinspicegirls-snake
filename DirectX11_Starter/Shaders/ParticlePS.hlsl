
// Input data from the GS
struct GStoPS
{
	float4 position : SV_POSITION;
	float4 color	: COLOR;
	float2 uv		: TEXCOORD;
};

// Texture data 
Texture2D particleTexture : register(t0);
SamplerState trilinear	  : register(s0); 


// PS entry 
float4 main(GStoPS input) : SV_TARGET
{
	// Tint the color of the texture from our interpulated color from the VS 
	return particleTexture.Sample(trilinear, input.uv) * input.color; 
}