#ifndef __DIRECTIONALLIGHT__
#define __DIRECTIONALLIGHT__

#include "ILight.hlsli"

class DirectionalLight : ILight
{
	float4 AmbientColor : COLOR0;
	float4 DiffuseColor : COLOR1;
	float4 Direction : NORMAL0;

	float4 ComputeAmbient(float3 worldPos, float3 normal)
	{
		return AmbientColor;
	}

	float4 ComputeDiffuse(float3 worldPos, float3 normal)
	{
		float3 lightDir = normalize(Direction.xyz);
		float lightNormalDot = saturate(dot(normal, -lightDir));

		return (DiffuseColor * lightNormalDot);
	}

	float4 ComputeColor(
		float4 surfaceAmbient,
		float4 surfaceDiffuse,
		float3 worldPos,
		float3 normal)
	{
		return (surfaceAmbient * ComputeAmbient(worldPos, normal) +
			surfaceDiffuse * ComputeDiffuse(worldPos, normal));
	}
};

#endif