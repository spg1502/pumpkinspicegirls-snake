float totalTime;
float tileWidth;					//Width of tiles before transformations are applied
float2 snakePosition;
float timePeriod;					//Controls how fast effects cycle. Higher values cycle slower
float floorScaleAmount;				//How much the floor gets scaled. Set to 0 to disable
float rotationScalar;				//How much the floor rotates around the snake. Set to 0 to disable. Sign controls direction

struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float3 normal		: NORMAL;
	float3 worldPos		: TEXCOORD0;
	float2 uv			: TEXCOORD1;
};

#include "DirectionalLight.hlsli"
#include "PointLight.hlsli"
#include "SpotLight.hlsli"

//Shader does use PointLights and SpotLights but not directional lights
//Using directional lights seemed unnecessary as the floor should be somewhat self illuminated
cbuffer LightArray : register(b0)
{
	DirectionalLight directionalLights[2];
	PointLight pointLights[1];
	SpotLight spotLights[9];
};

//These functions calculate the tile color
//Can be easily modified to change over time
float calculateRed(float radius, float time)
{
	radius = floor(radius);
	return (1 - (ceil(radius / 3) - floor(radius / 3)));
}
float calculateGreen(float radius, float time)
{
	radius = floor(radius) + 1;
	return (1 - (ceil(radius / 2) - floor(radius / 2)));
}
float calculateBlue(float radius, float time)
{
	radius = floor(radius);
	return (1 - (ceil(radius / 2) - floor(radius / 2)));
}
//Returns the source vector rotated about the center point
//Rotation is scaled down as the distance from the center increases
float2 rotateAboutPoint(float2 source, float2 center, float theta)
{
	float2 displacement = source - center;
	float distance = length(displacement);
	theta = theta / distance;
	float2x2 rotationMatrix = float2x2(cos(theta), -sin(theta), sin(theta), cos(theta));
	float2 rotationOffset = mul(rotationMatrix, displacement);
	return (source + rotationOffset);
}

float4 main(VertexToPixel input) : SV_TARGET
{
	//Set up some values that may be needed later on
	const float PI = 3.14159265;
	float2 worldPos = float2(input.worldPos.x, input.worldPos.z);
	float worldRadiusSquare = dot(worldPos, worldPos);
	float worldRadius = sqrt(worldRadiusSquare);
	
	//The elapsed time this period interpolated from 0 - 1
	//Forms a sawtooth wave
	float time = (totalTime / timePeriod) - floor(totalTime / timePeriod);
	float slowTime = (totalTime / (timePeriod * 2)) - floor(totalTime / (timePeriod * 2));
	//Elapsed time rectified and scaled to be a triangle wave varying from 0 - 1
	float rectifiedTime1 = 2 * abs(time - .5);
	//Rectified time with a 180 degree phase offset
	float rectifiedTime2 = -rectifiedTime1 + 1;

	//TRANSFORMATIONS//////////////////////////////////////////////////////////
	//New scaling code
	float scale = 1 + floorScaleAmount * sin(2 * PI * ((worldRadius / 8 + sin(rectifiedTime2)) - floor(worldRadius / 8 + sin(rectifiedTime2)))) / worldRadius;
	float2x2 scaleMatrix = float2x2(scale, 0, 0, scale);
	float2 scaledPos = mul(scaleMatrix, worldPos);

	//Rotate the floor around the snake
	float theta = rotationScalar;
	float2 adjustedPosition = rotateAboutPoint(scaledPos, snakePosition, theta) / tileWidth;

	//TILING///////////////////////////////////////////////////////////////////
	//Calculate distance from the center of the tile
	float radius = max(abs(adjustedPosition.x), abs(adjustedPosition.y));
	float2 tileCenter = float2(ceil(adjustedPosition.x) - .5, ceil(adjustedPosition.y) - .5);
	float2 centerDirection = tileCenter - adjustedPosition;
	float innerRadiusSquare = dot(centerDirection, centerDirection);
	float innerRadius = sqrt(innerRadiusSquare);

	//Determine brightness based on distance from the center of the tile
	float brightness = 1.0 - lerp(0, 1.25, (rectifiedTime1 + .2) * innerRadius * 2);
	float4 surfaceColor = float4(calculateRed(radius, rectifiedTime1) * brightness,
		calculateGreen(radius, rectifiedTime2) * brightness,
		calculateBlue(radius, rectifiedTime1) * brightness, brightness);

	float4 pixelColor = (0.0, 0.0, 0.0, 0.0);

	[unroll]
	for (uint i = 0; i < 2; i++)
	{
		pixelColor += directionalLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}
	[unroll]
	for (uint i = 0; i < 1; i++)
	{
		pixelColor += pointLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}
	[unroll]
	for (uint i = 0; i < 9; i++)
	{
		pixelColor += spotLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}

	return pixelColor;
	//Adjust brightness for lights
	/*
	return directionalLights[0].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal) +
		directionalLights[1].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal) +
		 pointLights[0].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal) +
		spotLights[0].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);*/
}
