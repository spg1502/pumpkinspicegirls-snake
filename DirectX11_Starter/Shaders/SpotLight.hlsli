#ifndef __SPOTLIGHT__
#define __SPOTLIGHT__

#include "ILight.hlsli"

class SpotLight : ILight
{
	float4 AmbientColor	: COLOR0;
	float4 DiffuseColor	: COLOR1;
	float3 Position	: POSITION0;
	float3 Direction : NORMAL0;
	float MaxAngle : PSIZE0;

	float4 ComputeAmbient(float3 worldPos, float3 normal)
	{
		return AmbientColor;
	}

	float4 ComputeDiffuse(float3 worldPos, float3 normal)
	{
		//First, calculate diffuse light as if it's a
		//Directional light, with direction being
		//light to point direction.
		float3 lightToPoint = worldPos - Position;
		float3 lightToPointDir = normalize(lightToPoint);
		float directionNormalDP = saturate(dot(normal, -lightToPointDir));

		//Now we'll lerp it based on the max angle of the spotlight
		float3 spotDirection = normalize(Direction);
		float dotProd = dot(lightToPointDir, spotDirection);
		float angleBetween = acos(dotProd);
		float angleFraction = angleBetween / MaxAngle;
		float fractionalAngle = 1 - angleFraction;
		float lerpVal = saturate(fractionalAngle);

		return (DiffuseColor * directionNormalDP * lerpVal);
	}

	float4 ComputeColor(
		float4 surfaceAmbient,
		float4 surfaceDiffuse,
		float3 worldPos,
		float3 normal)
	{
		return (surfaceAmbient * ComputeAmbient(worldPos, normal) +
			surfaceDiffuse * ComputeDiffuse(worldPos, normal));
	}
};

#endif