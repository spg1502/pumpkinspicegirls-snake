// Struct representing the data we expect to receive from earlier pipeline stages
// - Should match the output of our corresponding vertex shader
// - The name of the struct itself is unimportant
// - The variable names don't have to match other shaders (just the semantics)
// - Each variable must have a semantic, which defines its usage
struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float3 normal		: NORMAL;
	float3 worldPos		: TEXCOORD0;
	float2 uv			: TEXCOORD1;
};

#include "DirectionalLight.hlsli"
#include "PointLight.hlsli"
#include "SpotLight.hlsli"

cbuffer LightArray : register(b0)
{
	DirectionalLight directionalLights[2];
	PointLight pointLights[1];
	SpotLight spotLights[9];
};

Texture2D diffuseTexture : register(t0);
SamplerState basicSampler : register(s0);


// --------------------------------------------------------
// The entry point (main method) for our pixel shader
// 
// - Input is the data coming down the pipeline (defined by the struct)
// - Output is a single color (float4)
// - Has a special semantic (SV_TARGET), which means 
//    "put the output of this into the current render target"
// - Named "main" because that's the default the shader compiler looks for
// --------------------------------------------------------
float4 main(VertexToPixel input) : SV_TARGET
{
	//re normalize interpolated normal
	input.normal = normalize(input.normal);
	

	// Sample textures
	float4 surfaceColor = diffuseTexture.Sample(basicSampler, input.uv);

	float4 pixelColor = (0.0, 0.0, 0.0, 0.0);

	[unroll]
	for (uint i = 0; i < 2; i++)
	{
		pixelColor += directionalLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}
	[unroll]
	for (uint i = 0; i < 1; i++)
	{
		pixelColor += pointLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}
	[unroll]
	for (uint i = 0; i < 9; i++)
	{
		pixelColor += spotLights[i].ComputeColor(
			surfaceColor,
			surfaceColor,
			input.worldPos.xyz,
			input.normal);
	}

	return pixelColor;
}
