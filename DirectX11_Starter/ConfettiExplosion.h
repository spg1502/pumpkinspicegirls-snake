#pragma once
#include "ParticleSysInput.h"
#include "Particle.h"
#include "SimpleShader.h"
#include <d3d11.h>
#include <DirectXMath.h>


class ConfettiExplosion
{
public:
	ConfettiExplosion(
		ID3D11Device* device,
		BaseSystemPatricleData& pData,
		SimpleGeometryShader* spawnGS);
	~ConfettiExplosion();

	void DrawParticles(
		float deltaTime,
		ID3D11DeviceContext* deviceContext,
		SimpleVertexShader* baseVS,
		SimplePixelShader* basePS,
		SimpleGeometryShader* baseGS,
		SimpleVertexShader* spawnVS,
		SimpleGeometryShader* spawnGS);

	bool SystemDead();

private:

	// Lifetime variables
	float lifespan;
	float currentAge;

	// Buffer variables 
	ID3D11Buffer* particleVB;
	ID3D11Buffer* soBufferRead;
	ID3D11Buffer* soBufferWrite;
	bool firstFrame; 

	// Save own particle effect variables 
	BaseSystemPatricleData particleData; 

	// Helpers -----------------------------------

	// Initilization 
	void CreateVB(ID3D11Device* device);

	// Run time 
	void SwapSOBuffers();
	void ParticleSpawning(
		float deltaTime,
		ID3D11DeviceContext* deviceContext,
		SimpleVertexShader* spawnVS,
		SimpleGeometryShader* spawnGS);
};
