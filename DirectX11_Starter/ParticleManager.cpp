#include "ParticleManager.h"
#include <SimpleMath.h>
#include <time.h>
#include <WICTextureLoader.h>

#define ReleaseResource(x) { if(x){ x->Release(); x = 0; } }

ParticleManager::ParticleManager(
	ID3D11Device* device,
	ID3D11DeviceContext* deviceContext,
	wchar_t* confettiTexturePath)
{
	// Set up confetti data
	InitConfettiData(device, deviceContext, confettiTexturePath); 

	// Load shared data
	CreateRandomTexture(device);
	CreateBlendDepthStates(device); 
}


ParticleManager::~ParticleManager()
{
	// Delete shaders
	delete particleVS;
	delete particlePS;
	delete particleGS;
	delete particleSpawnVS;
	delete particleSpawnGS;

	// Release ID3D11 resources
	ReleaseResource(randomTexture);
	ReleaseResource(randomDataSRV);
	ReleaseResource(particleBlendState);
	ReleaseResource(particleDepthState);
	ReleaseResource(confettiTexture);
	ReleaseResource(particleSampler);

	// Clean up any left over particle effects 
	CleanUp(); 
}

void ParticleManager::SpawnConfettiEffect(DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 foodPos, ID3D11Device* device, ID3D11DeviceContext* deviceContext)
{
	// Update default data to hold new position
	defaultConfettiVariables.startPosition = position;

	// Get direction normalized and scale by force wanted
	DirectX::SimpleMath::Vector3 direction = foodPos - position; 
	direction.Normalize();
	direction *= DirectX::SimpleMath::Vector3(8.0f, 4.0f, 8.0f);

	// Set the velocity, make sure to set y on its own (comes out to 0 based on direction)
	defaultConfettiVariables.startVelocity = direction; 
	defaultConfettiVariables.startVelocity.y = 4.0f;


	// Push new confetti explosion onto the vector 
	activeConfettiEffect.push_back(new ConfettiExplosion(device, defaultConfettiVariables, particleSpawnGS));
}

void ParticleManager::Draw(
	float deltaTime,
	float totalTime,
	const DirectX::XMFLOAT4X4& view,
	const DirectX::XMFLOAT4X4& projection,
	ID3D11DeviceContext* deviceContext)
{
	// If there is a confetti effect, set any shared data ONCE per frame
	if (activeConfettiEffect.size() > 0)
	{
		// Set w-v-p matrices, same for all particles 
		particleGS->SetMatrix4x4("world", DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)); // Identity
		particleGS->SetMatrix4x4("view", view);
		particleGS->SetMatrix4x4("projection", projection);

		// Set texture and sampler for particle drawing 
		particlePS->SetSamplerState("trilinear", particleSampler);
		particlePS->SetShaderResourceView("particleTexture", confettiTexture);

		// Set shared data for particle spawing 
		particleSpawnGS->SetFloat("totalTime", totalTime);
		particleSpawnGS->SetSamplerState("randomSampler", particleSampler);
		particleSpawnGS->SetShaderResourceView("randomTexture", randomDataSRV);

		// Set blend/depth states 
		float factor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
		deviceContext->OMSetBlendState(particleBlendState, factor, 0xffffffff);
		deviceContext->OMSetDepthStencilState(particleDepthState, 0);

		// Set the topology to use 
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

		// Loop though all effect and clean up or draw 
		for (int i = 0; i < activeConfettiEffect.size(); ++i)
		{
			// Clean up any dead systems 
			if (activeConfettiEffect[i]->SystemDead())
			{
				delete activeConfettiEffect[i];
				activeConfettiEffect.erase(activeConfettiEffect.begin() + i);
			}
			// If still alive then draw 
			else
			{
				// Each effect will spawn particles, set unique data, and draw 
				activeConfettiEffect[i]->DrawParticles(
					deltaTime,
					deviceContext,
					particleVS,
					particlePS,
					particleGS,
					particleSpawnVS,
					particleSpawnGS);
			}
		}

		// Unset Geomerty shaders and reset blend/depth states after drawing all 
		deviceContext->GSSetShader(0, 0, 0);
		// Should reset the blend state, but the floor needs it and doesn't set it 
		//deviceContext->OMSetBlendState(0, factor, 0xffffffff);	
		deviceContext->OMSetDepthStencilState(0, 0);
	}
}


void ParticleManager::CleanUp()
{
	// Clean up any left over particle effects 
	for each(ConfettiExplosion* c in activeConfettiEffect)
	{
		delete c;
	}

	// Clear the vector to be safe 
	activeConfettiEffect.clear(); 
}


// ----------------------------------------------------------------------
// Helper functions 

void ParticleManager::InitConfettiData(ID3D11Device* device, ID3D11DeviceContext* deviceContext, wchar_t* texturePath)
{
	// Set confetti explosion vars 
	defaultConfettiVariables.startPosition = DirectX::XMFLOAT3(0, 0.0f, 0);
	defaultConfettiVariables.startVelocity = DirectX::XMFLOAT3(0.0f, 4.0f, 0.0f);
	defaultConfettiVariables.constAcceleration = DirectX::XMFLOAT3(0.0f, -1.5f, 0);
	defaultConfettiVariables.xzRandomFactor = 1.50f;

	defaultConfettiVariables.startColor = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 0.3f);
	defaultConfettiVariables.midColor = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 0.8f);
	defaultConfettiVariables.endColor = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
	defaultConfettiVariables.smeSizes = DirectX::XMFLOAT3(20, 30, 20);

	defaultConfettiVariables.ageToSpawn = 0.0f;
	defaultConfettiVariables.maxLifespan = 5.0f;
	defaultConfettiVariables.numParticles = 150;

	// Load texture 
	DirectX::CreateWICTextureFromFile(device, deviceContext, texturePath, 0, &confettiTexture);

	// Load particle shaders 
	particleVS = new SimpleVertexShader(device, deviceContext);
	particleVS->LoadShaderFile(L"../Assets/ParticleVS.cso");

	particlePS = new SimplePixelShader(device, deviceContext);
	particlePS->LoadShaderFile(L"../Assets/ParticlePS.cso");

	particleGS = new SimpleGeometryShader(device, deviceContext);
	particleGS->LoadShaderFile(L"../Assets/ParticleGS.cso");

	particleSpawnVS = new SimpleVertexShader(device, deviceContext);
	particleSpawnVS->LoadShaderFile(L"../Assets/PSpawnVS.cso");

	particleSpawnGS = new SimpleGeometryShader(device, deviceContext, true, false);
	particleSpawnGS->LoadShaderFile(L"../Assets/PSpawnGS.cso");
}

void ParticleManager::CreateRandomTexture(ID3D11Device* device)
{
	// Set up random data stuff 
	unsigned int randomTextureWidth = 1024;

	// Random data for the 1D texture 
	srand((unsigned int)time(0));
	std::vector<float> data(randomTextureWidth * 4);
	for (unsigned int i = 0; i < randomTextureWidth * 4; i++)
		data[i] = rand() / (float)RAND_MAX * 2.0f - 1.0f;

	// Set up the texture 
	D3D11_TEXTURE1D_DESC textureDesc;
	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.Width = 100;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = (void*)&data[0];
	initData.SysMemPitch = randomTextureWidth * sizeof(float) * 4;
	initData.SysMemSlicePitch = 0;
	device->CreateTexture1D(&textureDesc, &initData, &randomTexture);

	// Set up SRV for texture
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
	srvDesc.Texture1D.MipLevels = 1;
	srvDesc.Texture1D.MostDetailedMip = 0;
	device->CreateShaderResourceView(randomTexture, &srvDesc, &randomDataSRV);

	// Create the random texture sampler
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	device->CreateSamplerState(&samplerDesc, &particleSampler);
}

void ParticleManager::CreateBlendDepthStates(ID3D11Device* device)
{
	// Create an alpha blend state
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	device->CreateBlendState(&blendDesc, &particleBlendState);

	// Depth state
	D3D11_DEPTH_STENCIL_DESC depthDesc;
	ZeroMemory(&depthDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	depthDesc.DepthEnable = true;
	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	device->CreateDepthStencilState(&depthDesc, &particleDepthState);
}