#include "Entity.h"
#include <DirectXMath.h>
#include "DirectionalLightStruct.h"
#include "ShaderConstants.h"

using namespace DirectX::SimpleMath;

// ADDED default constrctor 
Entity::Entity()
	: m_model(nullptr),
	m_material(nullptr),
	transform()
{
}

Entity::Entity(Transform t)
	:transform{ t }
{

}

//The entity class that describes all of our visible game objects that have a model associated with them
Entity::Entity(
	Mesh* pModel, 
	Material* pMaterial,
	const Vector3& pPosition,
	const Quaternion& pRotation,
	const Vector3& pScale)
	: m_model(pModel),
	m_material(pMaterial),
	transform(pPosition, pRotation, pScale)
{
}

Entity::~Entity()
{
}

#pragma region Gets & Sets

const Transform& Entity::getTransform() const
{
	return transform; 
}
Transform& Entity::getTransform()
{
	return transform;
}

Material* Entity::GetMaterial() const
{
	return m_material;
}
void Entity::SetMaterial(Material* newMaterial)
{
	m_material = newMaterial;
}
Mesh* Entity::GetMesh() const
{
	return m_model;
}
void Entity::SetMesh(Mesh* newMesh)
{
	m_model = newMesh;
}
#pragma endregion

void Entity::Draw(ShaderConstants& shaderConstData)
{
	//Adding asserts so that failures in this section of code are explicitly mentioned.
	//These asserts are removed in release mode.
	assert(m_material != nullptr);
	assert(m_model != nullptr);
	assert(m_material->GetPixelShader() != nullptr);
	assert(m_material->GetVertexShader() != nullptr);

	DirectX::XMFLOAT4X4 transMatD = getTransform().GetMatrix();
	DirectX::XMMATRIX transMatM = XMLoadFloat4x4(&transMatD);
	transMatM = DirectX::XMMatrixTranspose(transMatM);
	DirectX::XMStoreFloat4x4(&shaderConstData.vShaderCBuffer.world, transMatM);

	m_material->PrepShaders(shaderConstData);

	m_model->DrawMesh();
}
