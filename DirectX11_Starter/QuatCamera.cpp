#include "QuatCamera.h"

using namespace DirectX;


//Default constructor will point in the negative z direction, is located at the origin
QuatCamera::QuatCamera(float aspectRatio)
	:Entity{ Transform{XMFLOAT3{0, 0, 0}} }, aspectRatio{ aspectRatio }
{
	upAxis = XMFLOAT3{ 0, 1.0f, 0 };
	rightAxis = XMFLOAT3{ 1.0f, 0, 0 };
	createViewMatrix();
	createProjectionMatrix();
}

QuatCamera::QuatCamera(float aspectRatio, Entity* e, float height)
	: Entity{ Transform{XMFLOAT3(e->getTransform().GetPosition())} }, aspectRatio{ aspectRatio }, entityToFollow{ e }, followHeight{ height }
{
	upAxis = XMFLOAT3{ 0, 1.0f, 0 };
	rightAxis = XMFLOAT3{ 1.0f, 0, 0 };
	createViewMatrix();
	createProjectionMatrix();
}


XMFLOAT4X4 QuatCamera::GetViewMatrix() { return viewMatrix; }
XMFLOAT4X4 QuatCamera::GetProjectionMatrix() { return projectionMatrix; }

void QuatCamera::SetNearClip(float dist) { nearPlaneDist = dist; createProjectionMatrix(); }
void QuatCamera::SetFarClip(float dist) { farPlaneDist = dist; createProjectionMatrix(); }
void QuatCamera::SetFOV(float fov) { FOV = fov; createProjectionMatrix(); }
void QuatCamera::SetAspectRatio(float r) { aspectRatio = r; createProjectionMatrix(); }

//Move forward, but stay in the XZ plane
void QuatCamera::MoveForward(float dist) 
{ 
	auto forward = transform.GetForwardVector();
	forward = XMVectorSetY(forward, 0);
	XMVector3Normalize(forward);
	transform.Translate(forward * dist); 
}
//Move right, staying in the XZ plane
void QuatCamera::MoveRight(float dist) 
{ 
	auto right = transform.GetRightVector();
	right = XMVectorSetY(right, 0);
	XMVector3Normalize(right);
	transform.Translate(right * dist); 
}
void QuatCamera::MoveUp(float dist)
{
	transform.Translate(XMLoadFloat3(&upAxis) * dist);
}
void QuatCamera::ToggleFPSMode() { fpsMode = !fpsMode; }
void QuatCamera::SetMoveSpeed(float speed) { movementSpeed = speed; }
void QuatCamera::SetEntityToFollow(Entity* e) { entityToFollow = e; }

void QuatCamera::RotateY(float angle)
{
	yaw += angle;
}
void QuatCamera::RotateX(float angle)
{
	pitch += angle;
	if (pitch > 80.0f) pitch = 80.0f;
	else if (pitch < -80.0f) pitch = -80.0f;
}

void QuatCamera::createProjectionMatrix()
{
	XMStoreFloat4x4(&projectionMatrix, XMMatrixPerspectiveFovLH(
		FOV,	
		aspectRatio,			
		nearPlaneDist,			
		farPlaneDist));			
}
void QuatCamera::createViewMatrix()
{
	//This applies the pitch rotation first then the yaw rotation
	auto yawRotation = XMQuaternionRotationAxis(XMLoadFloat3(&upAxis), yaw);
	auto pitchRotation = XMQuaternionRotationAxis(XMLoadFloat3(&rightAxis), pitch);
	auto totalRotation = XMQuaternionMultiply(pitchRotation, yawRotation);
	transform.SetRotation(totalRotation);

	auto position = transform.GetPosition();
	XMStoreFloat4x4(&viewMatrix, XMMatrixLookToLH(
		XMLoadFloat3(&position),
		transform.GetForwardVector(),
		transform.GetUpVector()));
}

void QuatCamera::Update(float dt)
{
	if (fpsMode)
	{
		//FPS controls
		//WASD moves in the XZ plane//Q and E move down and up 
		if (GetAsyncKeyState('W') & 0x8000) { MoveForward(dt * movementSpeed); }
		if (GetAsyncKeyState('S') & 0x8000) { MoveForward(dt * -movementSpeed); }
		if (GetAsyncKeyState('D') & 0x8000) { MoveRight(dt * movementSpeed); }
		if (GetAsyncKeyState('A') & 0x8000) { MoveRight(dt * -movementSpeed); }
		if (GetAsyncKeyState('E') & 0x8000) { MoveUp(dt * movementSpeed); }
		if (GetAsyncKeyState('Q') & 0x8000) { MoveUp(dt * -movementSpeed); }
	}
	else
	{
		//Follow an object if not in fps mode
		if (entityToFollow)
		{
			//Set camera to be directly above the target
			auto position = entityToFollow->getTransform().GetPosition();
			position.y = followHeight;
			transform.SetPosition(position);

			//Set camera to look down
			pitch = 1.57f;
			yaw = 0;
		}
	}
	createViewMatrix();
}

QuatCamera::~QuatCamera()
{
}
