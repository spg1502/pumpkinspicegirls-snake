#pragma once
#include "Entity.h"

class Camera :
	public Entity
{
public:
	Camera(const float& aspectRatio);
	Camera(const float& aspectRatio, Entity* entityToFollow, const float& height);
	~Camera();

	const DirectX::XMFLOAT4X4& GetViewMatrix();
	const DirectX::XMFLOAT4X4& GetProjectionMatrix();

	void SetEntityToFollow(Entity*);

	//Change projection matrix parameters
	void SetNearClip(const float& dist);
	void SetFarClip(const float& dist);
	void SetAspectRatio(const float& r);
	void SetFOV(const float& fov);
	void SetRotationRate(const float s);

	//Movement controls
	void SetMoveSpeed(const float& speed);

	void Update(const float& dt);

private:
	//Projection variables
	float FOV = .25 * 3.14159265;			//Camera comes with reasonable default values which can be changed at any time
	float nearPlaneDist = .1f;
	float farPlaneDist = 100.0f;
	float aspectRatio;

	Entity* entityToFollow;
	float followHeight = 20.0f;
	float pitch;
	float yaw;
	float movementSpeed = 5.0f;
	float lookSensitivity = .015f;
	float rotationRate = 2.0f;

	DirectX::XMFLOAT4X4 viewMatrix;
	DirectX::XMFLOAT4X4 projectionMatrix;
	DirectX::XMFLOAT3 upAxis;
	DirectX::XMFLOAT3 rightAxis;

	bool viewMatrixIsDirty;

	void createProjectionMatrix();
	void createViewMatrix();
};

