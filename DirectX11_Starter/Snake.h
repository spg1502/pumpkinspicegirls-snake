#pragma once

#include <deque>
#include "Entity.h"
#include "FoodPellet.h"
#include "Constants.h"

struct ShaderConstants;

// Snake will contain its own logic, just need to call Update and Draw
class Snake
{
public:
	Snake();
	// Needs to take in what Entity() needs 
	// Sepreate data for the head, body, and food 
	Snake(
		Mesh* headMesh,
		Mesh* bodyMesh,
		Mesh* foodMesh,
		Material* headMat,
		Material* bodyMat,
		Material* foodMat);
	~Snake();

	size_t GetLength() const;					// Returns the size of the snake
	DirectX::XMFLOAT3 GetHeadPos() const;		// Returns position of the snake's head
	DirectX::SimpleMath::Vector3 GetFoodPos() const;
	Entity* getHead();

	// Update returns an int based on what is happening to the snake 
	// 0 - Alive (nothing special)
	// 1 - Dead 
	// 2 - Picked up food this frame 
	int Update(float deltaTime);

float GetAngularVelocity() const;

	// Reset the snake to starting size and position 
	void Reset(); 

	void Draw(ShaderConstants& shaderConstData);

private:


	// EXTRA STUFF FOR ENTITY CREATION -- Should be changed
	std::vector<DirectionalLightStruct*> dLights;
	// Any intial data 
	const float INIT_BODY_SEGMENTS = 4;
	const float INIT_MOVE_DIRECTION = Constants::PI / 2; 
	const float HEAD_Y_DISPLACEMENT = .25; 

	// Play area - dance floor
	const float AREA_WIDTH = 45;
	const float AREA_LENGTH = 45; 

	// How many body parts to add per food 
	// Plus one to have < vs <=
	// Tested it out and did save some fps surprisingly 
	const float NUM_BODY_ADD_PLUS_ONE = 5;

	// An entity for each body part 
	std::deque<Entity> bodyComponents;
	Entity head;

	// Save mesh pointer for creating new body components 
	Mesh* bodyMesh;
	Material* bodyMat;
	Vector3 bodyScale;

	// Variables for movemnt / placement 
	Vector3 moveAmount;				// Forward movement per second 
	float deltaForwardAngle;		// Direction change per second (RADIANS) 
	float distanceBetweenPlacing;	// distance until next piece needs to be placed.
	float angularVelocity;			// Returns the angular velocity that the head is rotating at this frame

	//Var for speeding up Snake, in seconds
	float speedTime;

	// Food to collect 
	FoodPellet food;

	// Use for head movement 
	float headForwardAsAngle;
	Vector3 HeadAngleToForward();

	// Helper functions
	void KeyInput(float deltaTime);
	void Move(float deltaTime);
	void NewComponentAtEnd();	// Adds body piece at end of snake
	void NewComponentAtHead();	// Adds body piece at head of snake
	bool IsDead();				// True when the snake dies 

};
