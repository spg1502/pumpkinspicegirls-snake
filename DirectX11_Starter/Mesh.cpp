#include "Mesh.h"
#include "Vertex.h"
#include <d3d11.h>
#include <fstream>
#include <DirectXMath.h>

using namespace DirectX;

Mesh::Mesh(Vertex* pVertexArray, int pVerticesInVertexArray, unsigned int* pIndexArray, int pIndicesInIndexArray, ID3D11Device* pBufferCreator, ID3D11DeviceContext* pDeviceContext,
	int pNumVerts, int pOffsetToFirstIndex, int pOffsetStepSize)
{
	vertexArray = pVertexArray;
	verticesInVertexArray = pVerticesInVertexArray;
	indexArray = pIndexArray;
	indicesInIndexArray = pIndicesInIndexArray;
	bufferCreator = pBufferCreator;
	deviceContext = pDeviceContext;
	numVerts = pNumVerts;
	offsetToFirstIndex = pOffsetToFirstIndex;
	offsetStepSize = pOffsetStepSize;
	minPoint = XMFLOAT3(100000.0f, 100000.0f, 100000.0f);
	maxPoint = XMFLOAT3(-100000.0f, -100000.0f, -100000.0f);
	CreateBuffers();
}

Mesh::Mesh(char* filename, ID3D11Device* pBufferCreator, ID3D11DeviceContext* pDeviceContext)
{
	minPoint = XMFLOAT3(100000.0f, 100000.0f, 100000.0f);
	maxPoint = XMFLOAT3(-100000.0f, -100000.0f, -100000.0f);

	// File input object
	std::ifstream obj(filename);

	// Check for successful open
	if (!obj.is_open())
		return;

	//Variables used while reading the file
	std::vector<XMFLOAT3> positions;	// Positions from the file
	std::vector<XMFLOAT3> normals;		// Normals from the file
	std::vector<XMFLOAT2> uvs;			// UVs from the file
	std::vector<Vertex> verts;			// Verts we're assembling
	std::vector<UINT> indices;			// Indices of these verts
	unsigned int triangleCounter = 0;	// Count of triangles/indices
	char chars[100];					// String for line reading

	// Still good?
	while (obj.good())
	{
		// Get the line (100 characters should be more than enough)
		obj.getline(chars, 100);

		// Check the type of line
		if (chars[0] == 'v' && chars[1] == 'n')
		{
			// Read the 3 numbers directly into an XMFLOAT3
			XMFLOAT3 norm;
			sscanf_s(
				chars,
				"vn %f %f %f",
				&norm.x, &norm.y, &norm.z);

			// Add to the list of normals
			normals.push_back(norm);
		}
		else if (chars[0] == 'v' && chars[1] == 't')
		{
			// Read the 2 numbers directly into an XMFLOAT2
			XMFLOAT2 uv;
			sscanf_s(
				chars,
				"vt %f %f",
				&uv.x, &uv.y);

			// Add to the list of uv's
			uvs.push_back(uv);
		}
		else if (chars[0] == 'v')
		{
			// Read the 3 numbers directly into an XMFLOAT3
			XMFLOAT3 pos;
			sscanf_s(
					chars,
					"v %f %f %f",
					&pos.x, &pos.y, &pos.z);

			// Add to the positions
			positions.push_back(pos);

			maxPoint.x = std::fmaxf(pos.x, maxPoint.x);
			maxPoint.y = std::fmaxf(pos.y, maxPoint.y);
			maxPoint.z = std::fmaxf(pos.z, maxPoint.z);

			minPoint.x = std::fminf(pos.x, minPoint.x);
			minPoint.y = std::fminf(pos.y, minPoint.y);
			minPoint.z = std::fminf(pos.z, minPoint.z);

		}
		else if (chars[0] == 'f')
		{
			// Read the 9 face indices into an array
			unsigned int i[9];
			sscanf_s(
					chars,
					"f %d/%d/%d %d/%d/%d %d/%d/%d",
					&i[0], &i[1], &i[2],
					&i[3], &i[4], &i[5],
					&i[6], &i[7], &i[8]);

			// - Create the verts by looking up
			//    corresponding data from vectors
			// - OBJ File indices are 1-based, so
			//    they need to be adjusted
			Vertex v1;
			v1.Position = positions[i[0] - 1];
			v1.Normal = normals[i[2] - 1];
			v1.UV = uvs[i[1] - 1];

			Vertex v2;
			v2.Position = positions[i[3] - 1];
			v2.Normal = normals[i[5] - 1];
			v2.UV = uvs[i[4] - 1];

			Vertex v3;
			v3.Position = positions[i[6] - 1];
			v3.Normal = normals[i[8] - 1];
			v3.UV = uvs[i[7] - 1];

			// Add the verts to the vector
			verts.push_back(v1);
			verts.push_back(v2);
			verts.push_back(v3);

			// Add three more indices
			indices.push_back(triangleCounter++);
			indices.push_back(triangleCounter++);
			indices.push_back(triangleCounter++);
		}
	}

	//Close
	obj.close();

	// - At this point, "verts" is a vector of Vertex structs, and can be used
	//    directly to create a vertex buffer: &verts[0] is the first vert
	// - The vector "indices" is similar, It's a vector of unsigned ints and
	//    can be used directly for the index buffer: &indices[0] is the first int
	// - "triangleCounter" is BOTH the number of vertices and the number of indices

	//This only accounts for the first 3 vertecies, instead make it account for ALL the vertices in the mesh

	vertexArray = &verts[0];	//Can also set to verts.data when you cast it as a vertex array
	indexArray = &indices[0];
	bufferCreator = pBufferCreator;
	deviceContext = pDeviceContext;
	verticesInVertexArray = triangleCounter;
	indicesInIndexArray = triangleCounter;
	numVerts = triangleCounter;
	offsetToFirstIndex = 0;
	offsetStepSize = 0;
	CreateBuffers();
	//Then use that data to create new vertex and index buffers

	extents.x = (maxPoint.x - minPoint.x) * 0.5f;
	extents.y = (maxPoint.y - minPoint.y) * 0.5f;
	extents.z = (maxPoint.z - minPoint.z) * 0.5f;
}

Mesh::~Mesh()
{
	ReleaseMacro(vertexBufferPointer);
	ReleaseMacro(indexBufferPointer);

	delete vertexShader;
	delete pixelShader;
}

void Mesh::CreateBuffers()
{
	// Store the vertices of the triangle we are drawing
	// in a DirectX-controlled data structure
	D3D11_SUBRESOURCE_DATA initialVertexData;
	initialVertexData.pSysMem = vertexArray;

	// Create the VERTEX BUFFER description -----------------------------------
	// - The description is created on the stack because we only need
	//    it to create the buffer.  The description is then useless.
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex) * verticesInVertexArray;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER; // Tells DirectX this is a vertex buffer
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	// Actually create the buffer with the initial data
	// We're never changing the buffer again after this point
	HR(bufferCreator->CreateBuffer(&vbd, &initialVertexData, &vertexBufferPointer));

	// Create the proper struct to hold the initial index data
	// - This is how we put the initial data into the buffer
	D3D11_SUBRESOURCE_DATA initialIndexData;
	initialIndexData.pSysMem = indexArray;

	// Create the INDEX BUFFER description ------------------------------------
	// - The description is created on the stack because we only need
	//    it to create the buffer.  The description is then useless.
	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(int) * indicesInIndexArray;         // 3 = number of indices in the buffer
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER; // Tells DirectX this is an index buffer
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;

	// Actually create the buffer with the initial data
	// - Once we do this, we'll NEVER CHANGE THE BUFFER AGAIN
	HR(bufferCreator->CreateBuffer(&ibd, &initialIndexData, &indexBufferPointer));
}

void Mesh::SetBuffers()
{
	// Set buffers in the input assembler
	//  - Do this ONCE PER OBJECT you're drawing, since each object might
	//    have different geometry.
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	deviceContext->IASetVertexBuffers(0, 1, &vertexBufferPointer, &stride, &offset);
	deviceContext->IASetIndexBuffer(indexBufferPointer, DXGI_FORMAT_R32_UINT, 0);

	// Need to set what topology to use 
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Mesh::DrawIndexedMesh()
{
	// Finally do the actual drawing
	//  - Do this ONCE PER OBJECT you intend to draw
	//  - This will use all of the currently set DirectX "stuff" (shaders, buffers, etc)
	//  - DrawIndexed() uses the currently set INDEX BUFFER to look up corresponding
	//     vertices in the currently set VERTEX BUFFER
	deviceContext->DrawIndexed(
		numVerts,				// The number of indices to use (we could draw a subset if we wanted)
		offsetToFirstIndex,     // Offset to the first index we want to use
		offsetStepSize);		// Offset to add to each index when looking up vertices
}

void Mesh::DrawMesh()
{
	SetBuffers();
	DrawIndexedMesh();
}

ID3D11Buffer* Mesh::GetVertexBuffer()
{
	return vertexBufferPointer;
}

ID3D11Buffer* Mesh::GetIndexBuffer()
{
	return indexBufferPointer;
}

int Mesh::GetIndexCount()
{
	return indexBufferIndices;
}

XMFLOAT3 Mesh::GetExtents() const
{
	return extents;
}

DirectX::XMFLOAT3 Mesh::MinPoint() const
{
	return minPoint;
}

DirectX::XMFLOAT3 Mesh::MaxPoint() const
{
	return maxPoint;
}
