#pragma once

#include <SimpleMath.h>
#include "Entity.h"
#include "Physics.h"

using namespace DirectX::SimpleMath;

enum PelletType
{
	Standard,
	Speed
};

class FoodPellet
{
public:
	static void InitMaterials(Material* redMaterial, Material* greenMaterial);

	FoodPellet();
	FoodPellet(
		Mesh* mesh, 
		Material* material, 
		float width, 
		float height);	
	~FoodPellet();
	
	bool DidCollide(float r, const Vector3& p) const;

	void Reset();

	void Draw(ShaderConstants& shaderConstData);

	PelletType getType() const;

	void SetType(PelletType newType);

	DirectX::SimpleMath::Vector3 GetPos() const; 

private:

	Entity entity;

	const float RADIUS = 0.75f;
	const float Y_DISPLACEMENT = .5;

	// Area where the food call spawn 
	float width;
	float height;

	PelletType type;

	static Material* RedSoloCupTexture;
	static Material* GreenSoloCupTexture;
};
