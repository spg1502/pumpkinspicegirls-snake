#include "ConfettiExplosion.h"
#include <time.h>
#include <WICTextureLoader.h>

#define ReleaseResource(x) { if(x){ x->Release(); x = 0; } }


ConfettiExplosion::ConfettiExplosion(
	ID3D11Device* device,
	BaseSystemPatricleData& pData,
	SimpleGeometryShader* spawnGS) :
	particleData(pData)
{
	// Set the max age
	lifespan = particleData.maxLifespan + 1;

	// Using for spawning
	firstFrame = true;

	// Create SO buffers in the GS
	spawnGS->CreateCompatibleStreamOutBuffer(&soBufferRead, particleData.numParticles);
	spawnGS->CreateCompatibleStreamOutBuffer(&soBufferWrite, particleData.numParticles);

	CreateVB(device);
}

ConfettiExplosion::~ConfettiExplosion()
{
	// Release ID3D11 resources 
	ReleaseResource(particleVB);
	ReleaseResource(soBufferRead);
	ReleaseResource(soBufferWrite);
}



void ConfettiExplosion::DrawParticles(
	float deltaTime,
	ID3D11DeviceContext* deviceContext,
	SimpleVertexShader* baseVS,
	SimplePixelShader* basePS,
	SimpleGeometryShader* baseGS,
	SimpleVertexShader* spawnVS,
	SimpleGeometryShader* spawnGS)
{
	// When you draw, age the system as a whole
	currentAge += deltaTime;

	// Handle spawning particles 
	ParticleSpawning(deltaTime, deviceContext, spawnVS, spawnGS);

	// Data that could be unique per system
	baseVS->SetFloat3("acceleration", particleData.constAcceleration);
	baseVS->SetFloat("maxLifetime", particleData.maxLifespan);

	// Set "drawing" shaders, must be set after spawer shaders are done
	baseVS->SetShader(true);
	basePS->SetShader(true);
	baseGS->SetShader(true);

	// Set buffers 
	UINT particleStride = sizeof(ParticleVertex);
	UINT particleOffset = 0;
	deviceContext->IASetVertexBuffers(0, 1, &soBufferRead, &particleStride, &particleOffset);

	// Draw auto - based on current stream out buffer 
	deviceContext->DrawAuto();
}


// Helpers -------------------------------------------------------

// Initilization 
void ConfettiExplosion::CreateVB(ID3D11Device* device)
{
	// Vertices to put into the vertex buffer 
	ParticleVertex vertices[1];

	// Set the particle data 
	vertices[0].Type = 0;
	vertices[0].Age = 0.0f;
	vertices[0].StartPosition = particleData.startPosition;
	vertices[0].StartVelocity = particleData.startVelocity;
	vertices[0].StartColor = particleData.startColor;
	vertices[0].MidColor = particleData.midColor;
	vertices[0].EndColor = particleData.endColor;
	vertices[0].StartMidEndSize = particleData.smeSizes;

	// Create the VB 
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(ParticleVertex);
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA initialVertexData;
	initialVertexData.pSysMem = vertices;
	device->CreateBuffer(&vbd, &initialVertexData, &particleVB);
}


// Swap stream out buffers for ping-ponging 
void ConfettiExplosion::SwapSOBuffers()
{
	ID3D11Buffer* temp = soBufferRead;
	soBufferRead = soBufferWrite;
	soBufferWrite = temp;
}

void ConfettiExplosion::ParticleSpawning(
	float deltaTime,
	ID3D11DeviceContext* deviceContext,
	SimpleVertexShader* spawnVS,
	SimpleGeometryShader* spawnGS)
{
	UINT stride = sizeof(ParticleVertex);
	UINT offset = 0;

	// Set spawn data unique to each system 
	spawnGS->SetFloat("dt", deltaTime);
	spawnGS->SetFloat("ageToSpawn", particleData.ageToSpawn);
	spawnGS->SetFloat("maxLifetime", particleData.maxLifespan);
	spawnGS->SetFloat("xzRandomFactor", particleData.xzRandomFactor);

	// Set shaders to use for spawning 
	spawnVS->SetShader();
	spawnGS->SetShader();

	// Not using a PS for the spawn
	deviceContext->PSSetShader(0, 0, 0);

	// Unbind the vertex buffers 
	ID3D11Buffer* unset = 0;
	deviceContext->IASetVertexBuffers(0, 1, &unset, &stride, &offset);

	// On first frame 
	if (firstFrame)
	{
		// Draw using the seed vertex 
		deviceContext->IASetVertexBuffers(0, 1, &particleVB, &stride, &offset);
		deviceContext->SOSetTargets(1, &soBufferWrite, &offset);
		deviceContext->Draw(1, 0);
		firstFrame = false;
	}
	// After that
	else
	{
		//Draw using the buffers 
		deviceContext->IASetVertexBuffers(0, 1, &soBufferRead, &stride, &offset);
		deviceContext->SOSetTargets(1, &soBufferWrite, &offset);
		deviceContext->DrawAuto();
	}

	// Unbind SO targets and shaders 
	SimpleGeometryShader::UnbindStreamOutStage(deviceContext);
	deviceContext->GSSetShader(0, 0, 0);

	// Swap after draw
	SwapSOBuffers();
}

bool ConfettiExplosion::SystemDead()
{
	return currentAge > lifespan; 
}