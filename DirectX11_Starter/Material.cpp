#include "Material.h"

#include "ShaderConstants.h"


// Saves shader pointers and creates the sampler 
Material::Material(SimpleVertexShader* vertexShader, SimplePixelShader* pixelShader, ID3D11Device* device) : vertexShader(vertexShader), pixelShader(pixelShader)
{
	// Sampler description struct 
	D3D11_SAMPLER_DESC sDesc;
	ZeroMemory(&sDesc, sizeof(sDesc));
	sDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the sampler
	device->CreateSamplerState(&sDesc, &sampler);
}


Material::~Material()
{
	// Make sure to release texture data memory on exit 
	// Null check first, becuase all maps may not be loaded  
	if (diffuseMap != nullptr) diffuseMap->Release();
	if (specularMap != nullptr) specularMap->Release();
	if (normalMap != nullptr) normalMap->Release();
	if (sampler != nullptr) sampler->Release();
}


SimpleVertexShader* Material::GetVertexShader() const
{
	return vertexShader;
}

SimplePixelShader* Material::GetPixelShader() const 
{
	return pixelShader;
}



// Load a diffuse map from a file path 
void Material::LoadDiffuseMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath)
{
	DirectX::CreateWICTextureFromFile(pDevice, pContext, filePath, nullptr, &diffuseMap);
}

// Load a specular map from a file path 
void Material::LoadSpecularMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath)
{
	DirectX::CreateWICTextureFromFile(pDevice, pContext, filePath, nullptr, &specularMap);

}

// Load a normal map from a file path 
void Material::LoadNormalMap(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, wchar_t* filePath)
{
	DirectX::CreateWICTextureFromFile(pDevice, pContext, filePath, nullptr, &normalMap);
}

const ID3D11ShaderResourceView * Material::getDiffuseMap() const
{
	return diffuseMap;
}

ID3D11ShaderResourceView * Material::getDiffuseMap()
{
	return diffuseMap;
}

const ID3D11ShaderResourceView * Material::getSpecularMap() const
{
	return specularMap;
}

ID3D11ShaderResourceView * Material::getSpecularMap()
{
	return specularMap;
}

const ID3D11ShaderResourceView * Material::getNormalMap() const
{
	return normalMap;
}

ID3D11ShaderResourceView * Material::getNormalMap()
{
	return normalMap;
}

void Material::setDiffuseMap(ID3D11ShaderResourceView * newDiffuseMap)
{
	diffuseMap = newDiffuseMap;
}

void Material::setSpecularMap(ID3D11ShaderResourceView * newSpecularMap)
{
	specularMap = newSpecularMap;
}

void Material::setNormalMap(ID3D11ShaderResourceView * newNormalMap)
{
	normalMap = newNormalMap;
}


// Sends nessary data to shaders and sets shaders for next draw call 
void Material::PrepShaders(ShaderConstants& shaderConstData)
{
	shaderConstData.pShaderCBuffer.diffuseTexture = diffuseMap;
	shaderConstData.pShaderCBuffer.basicSampler = sampler;
	shaderConstData.LoadIntoShaders(*vertexShader, *pixelShader);
	// Send transfrom matrix to the vertex shader 

	// Use shaders on next draw call 
	vertexShader->SetShader(true);
	pixelShader->SetShader(true);

	vertexShader->CopyAllBufferData();
}