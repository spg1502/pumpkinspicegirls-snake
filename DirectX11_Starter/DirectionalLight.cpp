#include "DirectionalLight.h"
#include "Entity.h"
#include "Constants.h"
#include <DirectXMath.h>

using namespace DirectX;

DirectionalLight::DirectionalLight()
{
	// Does nothing, but is here so you don't get errors for making on the stack
}

DirectionalLight::DirectionalLight(Entity e)
	:Entity{ e }
{

}

DirectionalLight::DirectionalLight(
	DirectX::SimpleMath::Color pAmbientColor,
	DirectX::SimpleMath::Color pDiffuseColor,
	DirectX::SimpleMath::Quaternion pRotation) :
	ambientColor(pAmbientColor),
	diffuseColor(pDiffuseColor)
{
	XMVECTOR rotation = XMLoadFloat4(&pRotation);
	transform.SetRotation(rotation);
}

DirectionalLight::DirectionalLight(
	const DirectX::SimpleMath::Color& pAmbientColor,
	const DirectX::SimpleMath::Color& pDiffuseColor,
	const DirectX::SimpleMath::Vector3& pDirection) :
	ambientColor(pAmbientColor),
	diffuseColor(pDiffuseColor)
{
	//Set up vectors to calculate quaternion between
	auto forward = DirectX::SimpleMath::Vector3::Forward;
	auto direction = pDirection;
	direction.Normalize();

	//Use cross product to determine axis to rotate around
	//a cross b = ||a|| * ||b|| * sin(theta)n where n is a unit vector orthogonal to a and b
	DirectX::SimpleMath::Vector3 axis = direction.Cross(forward);
	DirectX::SimpleMath::Quaternion q;
	q.x = axis.x;
	q.y = axis.y;
	q.z = axis.z;
	q.w = 1 + direction.Dot(forward);

	//q is currently <1 + cos(theta), xsin(theta), ysin(theta), zsin(theta)>
	//correct rotation is <cos(theta/2), xsin(theta/2), ysin(theta/2), zsin(theta/2)> where x is the axis
	//Normalizing will fix this
	q.Normalize();
	transform.SetRotation(q);
}

DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::setAmbientColor(const DirectX::SimpleMath::Color& pAmbientColor)
{
	ambientColor = pAmbientColor;
}

void DirectionalLight::setDiffuseColor(const DirectX::SimpleMath::Color& pDiffuseColor)
{
	diffuseColor = pDiffuseColor;
}

void DirectionalLight::setDirection(const DirectX::SimpleMath::Quaternion& pDirection)
{
	XMVECTOR rotation = XMLoadFloat4(&pDirection);
	transform.SetRotation(rotation);
}

void DirectionalLight::loadData(PixelShaderCBuffer::DirectionalLight &dataContainer)
{
	dataContainer.ambientColor = ambientColor;
	dataContainer.diffuseColor = diffuseColor;
	dataContainer.direction = getDirection();
}

const DirectX::SimpleMath::Color& DirectionalLight::getAmbientColor() const
{
	return ambientColor;
}

const DirectX::SimpleMath::Color& DirectionalLight::getDiffuseColor() const
{
	return diffuseColor;
}

DirectX::SimpleMath::Vector3 DirectionalLight::getDirection() const
{
	DirectX::SimpleMath::Vector3 direction;
	XMStoreFloat3(&direction, transform.GetForwardVector());
	return direction;
}

void DirectionalLight::Update(float totalTime)
{
	DirectX::SimpleMath::Color newDiffuseColor = getDiffuseColor();
	if (newDiffuseColor.x == 1.0f)
	{
		if (newDiffuseColor.y == 1.0f)
		{
			newDiffuseColor.x -= .005f;
			newDiffuseColor.z += .005f;
		}
		else
		{
			newDiffuseColor.y += .005f;
			newDiffuseColor.z -= .005f;
		}
	}

	else if (newDiffuseColor.y == 1.0f)
	{
		if (newDiffuseColor.z == 1.0f)
		{
			newDiffuseColor.y -= .005f;
			newDiffuseColor.x += .005f;
		}
		else
		{
			newDiffuseColor.x -= .005f;
			newDiffuseColor.z += .005f;
		}
	}

	else if (newDiffuseColor.z == 1.0f)
	{
		if (newDiffuseColor.x == 1.0f)
		{
			newDiffuseColor.z -= .005f;
			newDiffuseColor.y += .005f;
		}
		else
		{
			newDiffuseColor.x += .005f;
			newDiffuseColor.y -= .005f;
		}
	}

	else
	{
		newDiffuseColor.x = 1.0f;
		newDiffuseColor.y = 1.0f;
		newDiffuseColor.z = 0.0f;
	}
	
	newDiffuseColor.Saturate();
	setDiffuseColor(newDiffuseColor);
}
