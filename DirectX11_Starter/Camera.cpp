#include "Camera.h"

using namespace DirectX;


//Place the camera at the origin, and start it in first person mode
Camera::Camera(const float& aspectRatio)
	:Entity{ Transform{ XMFLOAT3{ 0, 0, 0 } } }, aspectRatio{ aspectRatio }
{
	upAxis = XMFLOAT3{ 0, 1.0f, 0 };
	rightAxis = XMFLOAT3{ 1.0f, 0, 0 };
	createViewMatrix();
	createProjectionMatrix();
}
//Camera will be set to look at the Entity, and hover over it at the specified height
Camera::Camera(const float& aspectRatio, Entity* e, const float& height)
	: Entity{ Transform{ XMFLOAT3(e->getTransform().GetPosition()) } }, aspectRatio{ aspectRatio }, entityToFollow{ e }, followHeight{ height }
{
	upAxis = XMFLOAT3{ 0, 1.0f, 0 };
	rightAxis = XMFLOAT3{ 1.0f, 0, 0 };
	createViewMatrix();
	createProjectionMatrix();
}


const XMFLOAT4X4& Camera::GetViewMatrix() { return viewMatrix; }
const XMFLOAT4X4& Camera::GetProjectionMatrix() { return projectionMatrix; }

//Methods that edit the projection matrix will automatically rebuild it 
void Camera::SetNearClip(const float& dist) { nearPlaneDist = dist; createProjectionMatrix(); }
void Camera::SetFarClip(const float& dist) { farPlaneDist = dist; createProjectionMatrix(); }
void Camera::SetFOV(const float& fov) { FOV = fov; createProjectionMatrix(); }
void Camera::SetAspectRatio(const float& r) { aspectRatio = r; createProjectionMatrix(); }

void Camera::SetMoveSpeed(const float& speed) { movementSpeed = speed; }
void Camera::SetEntityToFollow(Entity* e) { entityToFollow = e; }
void Camera::SetRotationRate(float s) { rotationRate = s; }

void Camera::createProjectionMatrix()
{
	XMStoreFloat4x4(&projectionMatrix, XMMatrixPerspectiveFovLH(
		FOV,
		aspectRatio,
		nearPlaneDist,
		farPlaneDist));
}
void Camera::createViewMatrix()
{
	auto position = transform.GetPosition();
	XMStoreFloat4x4(&viewMatrix, XMMatrixLookToLH(
		XMLoadFloat3(&position),
		transform.GetForwardVector(),
		transform.GetUpVector()));
}

void Camera::Update(const float& dt)
{
	//Follow an object if not in fps mode
	if (entityToFollow)
	{
		//Set the camera rotation to follow the snake
		XMFLOAT4 currentRotation = transform.GetRotation();
		XMFLOAT4 snakeRotation = entityToFollow->getTransform().GetRotation();
		XMVECTOR targetRotation = XMLoadFloat4(&snakeRotation);
		targetRotation = XMQuaternionMultiply(targetRotation, XMQuaternionRotationAxis(entityToFollow->getTransform().GetRightVector(), XM_PIDIV4));

		//Do a smooth interpolation between current and target orientations
		transform.SetRotation(XMQuaternionSlerp(XMLoadFloat4(&currentRotation), targetRotation, dt * rotationRate));

		//Set position so that the snake will be in the middle of the screen while the player rotates the camera around it
		auto position = entityToFollow->getTransform().GetPosition();
		auto tempPosition = XMLoadFloat3(&position);
		tempPosition = tempPosition - followHeight * transform.GetForwardVector();
		XMStoreFloat3(&position, tempPosition);
		transform.SetPosition(position);
	}
	createViewMatrix();
}

Camera::~Camera()
{
}
