#pragma once
#include "UIElement.h"
#include <string>
#include <SpriteFont.h>

class UIButton :
	public UIElement
{
public:
	UIButton(std::shared_ptr<Texture> texture, std::shared_ptr<DirectX::SpriteFont> spriteFont, wchar_t* text,
		Vector2 position, float rotation, Vector2 scale);
	~UIButton();

	virtual void Draw(std::unique_ptr<DirectX::SpriteBatch> const &spriteBatch);
	inline bool Clicked(int x, int y) { return x >= bounds.left && x <= bounds.right && y >= bounds.top && y <= bounds.bottom; }
	void SetText(wchar_t* text);

private:
	std::shared_ptr<DirectX::SpriteFont> spriteFont;
	wchar_t* text;
	Vector2 textOrigin;
};

