#pragma once
#include "Entity.h"
#include "ShaderConstants.h"

class SpotLight :
	public Entity
{
public:
	SpotLight();
	SpotLight(
		const Transform& transform,
		const DirectX::SimpleMath::Color& ambientColor,
		const DirectX::SimpleMath::Color& diffuseColor,
		float maxAngle);
	SpotLight(const Transform& transform);
	virtual ~SpotLight();

	const DirectX::SimpleMath::Color& getAmbientColor() const;
	const DirectX::SimpleMath::Color& getDiffuseColor() const;
	float getMaxAngle() const;

	void setAmbientColor(const DirectX::SimpleMath::Color& newAmbientColor);
	void setDiffuseColor(const DirectX::SimpleMath::Color& newDiffuseColor);
	void setMaxAngle(float newAngle);

	void sweepX(float deltaTime, float speed, float maxDeflection);
	void sweepZ(float deltaTime, float speed, float maxDeflection);
	void passiveSpin(float deltaTime);
	void passiveRecolor(float totalTime);

	void loadData(PixelShaderCBuffer::SpotLight& dataContainer) const;

private:
	DirectX::SimpleMath::Color m_ambientColor;
	DirectX::SimpleMath::Color m_diffuseColor;
	float m_maxAngle;
	float m_totalTime;
};

