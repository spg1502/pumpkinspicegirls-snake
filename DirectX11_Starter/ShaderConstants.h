#pragma once
#include <DirectXMath.h>
#include "SimpleMath.h"
#include <d3d11.h>

class SimpleVertexShader;
class SimplePixelShader;

struct PixelShaderCBuffer
{
	struct DirectionalLight
	{
		DirectX::SimpleMath::Color ambientColor;
		DirectX::SimpleMath::Color diffuseColor;
		DirectX::SimpleMath::Vector3 direction;
		float padding0;
	};

	struct PointLight
	{
		DirectX::SimpleMath::Color ambientColor;
		DirectX::SimpleMath::Color diffuseColor;
		DirectX::SimpleMath::Vector3 position;
		float padding0;
	};

	struct SpotLight
	{
		DirectX::SimpleMath::Color ambientColor;
		DirectX::SimpleMath::Color diffuseColor;
		DirectX::SimpleMath::Vector3 position;
		float padding0;
		DirectX::SimpleMath::Vector3 direction;
		//No padding here, because I dunno.
		float maxAngle;
	};

	DirectionalLight directionalLights[2];
	PointLight pointLights[1];
	SpotLight spotLights[9];

	ID3D11ShaderResourceView* diffuseTexture;
	ID3D11SamplerState* basicSampler;

	bool LoadIntoShader(SimplePixelShader& pixelShader);
};

struct VertexShaderCBuffer
{
	DirectX::SimpleMath::Matrix world;
	DirectX::SimpleMath::Matrix view;
	DirectX::SimpleMath::Matrix projection;

	bool LoadIntoShader(SimpleVertexShader& vertexShader);
};

struct ShaderConstants
{
	PixelShaderCBuffer pShaderCBuffer;
	VertexShaderCBuffer vShaderCBuffer;

	ShaderConstants();

	bool LoadIntoShaders(SimpleVertexShader& vertexShader, SimplePixelShader& pixelShader);
};