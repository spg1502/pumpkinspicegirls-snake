#pragma once

#include <DirectXMath.h>
#include "DirectXGameCore.h"
#include "SimpleShader.h"
#include "Mesh.h"
#include "Entity.h"
#include "Camera.h"
#include <vector>
#include "DirectionalLight.h"
#include "QuatCamera.h"
#include "Snake.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <memory>
#include <wrl\client.h>
#include "Texture.h"
#include "UIButton.h"
#include <CommonStates.h>
#include "ShaderConstants.h"
#include "SpotLight.h"
#include "Animation.h"
#include "ParticleManager.h"
#include "PointLight.h"

// Include run-time memory checking in debug builds, so 
// we can be notified of memory leaks
#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

// --------------------------------------------------------
// Game class which extends the base DirectXGameCore class
// --------------------------------------------------------
class MyDemoGame : public DirectXGameCore
{
public:
	MyDemoGame(HINSTANCE hInstance);
	~MyDemoGame();

	// Overrides for base level methods
	bool Init();
	void OnResize();
	void UpdateScene(float deltaTime, float totalTime);
	void DrawScene(float deltaTime, float totalTime);

	// For handing mouse input
	void OnMouseDown(WPARAM btnState, int x, int y);
	void OnMouseUp(WPARAM btnState, int x, int y);
	void OnMouseMove(WPARAM btnState, int x, int y);

private:
	// Initialization for our "game" demo - Feel free to
	// expand, alter, rename or remove these once you
	// start doing something more advanced!
	void LoadShaders();
	void CreateGeometry();
	void CreateMatrices();
	void LoadTexture(const wchar_t* file, std::shared_ptr<Texture> tex);

	void UpdateMenu(float deltaTime, float totalTime);
	void UpdateGame(float deltaTime, float totalTime);

	void SetupUI();
	void CheckButtons();
	bool IsOutOfBounds();

	std::vector<DirectionalLight> dLights;
	std::vector<SpotLight> sLights;
	std::vector<PointLight> pLights;

	Camera* camera;

	boolean mouseDown;

	//Texture varibles that let us put information into the shader
	std::shared_ptr<Texture> start_tex;
	std::shared_ptr<Texture> exit_tex;
	std::shared_ptr<Texture> backgroundTex;
	std::shared_ptr<Texture> bgFloogTex;

	Material* material1;

	//Solo cup objects
	Material* redSoloCupMat;
	Material* greenSoloCupMat;
	Material* soloCupHeadMat;
	Mesh* soloCupMesh; 
	Mesh* soloCupGroupMesh; 


	Material* floorMat;
	Material* floorBgMat;
	Mesh* floorMesh; 
	Entity floor;
	std::unique_ptr<Entity> floorBg;
	
private:

	// Wrappers for DirectX shaders to provide simplified functionality
	SimpleVertexShader* vertexShader;
	SimplePixelShader* pixelShader;
	SimplePixelShader* floorShader;

	// The matrices to go from model space to screen space
	DirectX::XMFLOAT4X4 worldMatrix;
	DirectX::XMFLOAT4X4 viewMatrix;
	DirectX::XMFLOAT4X4 projectionMatrix;

	// Keeps track of the old mouse position.  Useful for 
	// determining how far the mouse moved in a single frame.
	POINT prevMousePos;

	enum GameStates
	{
		mainMenu,
		gamePlay,
		end
	};

	GameStates currentGameState;
	bool paused;
	float floorRotation;				//Angle that the floor should rotate. Set based on the snakes turning

	Snake* snake; 
	std::unique_ptr<DirectX::SpriteBatch> spriteBatch;
	std::shared_ptr<DirectX::SpriteFont> spriteFont;

	//Common States
	std::unique_ptr<DirectX::CommonStates> m_states;

	//UI buttons
	std::unique_ptr<UIButton> startBtn;
	std::unique_ptr<UIButton> exitBtn;
	std::unique_ptr<UIButton> restartBtn;
	std::unique_ptr<UIButton> mainMenuBtn;

	//Background
	std::unique_ptr<Animation> backGround;

	// Particles
	ParticleManager* particleManager; 
};
